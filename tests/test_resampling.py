import os
import tempfile
import unittest
from glob import glob

import h5py
import numpy
from casacore.tables import table as ms_table

from lofar_quality.common import INSPECT_DS_VERSION
from lofar_quality.create.v1 import derive_observation_metadata, derive_reference_frame, \
    derive_pointing, get_channel_frequencies, get_baselines, extract_flags_dataloss, \
    get_time_samples, resample_data, resample_uvw

TEST_DATA_DIRECTORY = os.path.join(os.path.dirname(__file__), 'data')


def list_test_data_ms():
    return glob(os.path.join(TEST_DATA_DIRECTORY, '*.MS'))


def is_input_data_available():
    return len(list_test_data_ms()) > 1


def extract_flags_dataloss_numpy(ms_path):
    n_antennas = ms_table(f'{ms_path}::ANTENNA').nrows()
    main_table = ms_table(ms_path)
    n_baselines = int((n_antennas + 1) * n_antennas * .5)
    flags = main_table.getcol('FLAG')
    uvw = main_table.getcol('UVW')
    n_times = flags.shape[0] // n_baselines
    vis = main_table.getcol('DATA')
    dataloss = numpy.array(vis == 0)
    flags = flags.reshape((n_times, n_baselines, *flags.shape[1:]), order='C').astype(
        bool)
    dataloss = dataloss.reshape((n_times, n_baselines, *dataloss.shape[1:]),
                                order='C').astype(bool)
    uvw = uvw.reshape((n_times, n_baselines, *uvw.shape[1:]), order='C')
    return flags, dataloss, uvw


class ResamplingTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.ms_list = list_test_data_ms()
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.tmpdir_path = self.tmp_dir.name

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()

    def _test_ms(self, index, ms_path):
        time_resolution = 60
        input_path = ms_path

        output_path = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
        inspect_ds = h5py.File(output_path, 'w')
        inspect_ds.attrs['VERSION'] = INSPECT_DS_VERSION
        inspect_ds.create_group('DATASET')
        dataset = inspect_ds['/DATASET']
        dataset.attrs.update(derive_observation_metadata(input_path))
        dataset.attrs.update(derive_pointing(input_path))
        dataset.attrs.update(derive_reference_frame(input_path))
        dataset['FREQUENCIES'] = get_channel_frequencies(input_path)
        dataset['BASELINES'] = get_baselines(input_path)

        time_axis = get_time_samples(input_path)

        # New method
        flag, dataloss, uvw = extract_flags_dataloss(dataset, input_path)
        flag, _, _ = resample_data(time_resolution, time_axis, flag)
        dataloss, resample_time_axis, total_counts = resample_data(time_resolution,
                                                                   time_axis, dataloss)
        uvw, *_ = resample_uvw(time_resolution, time_axis, uvw)

        # Numpy in memory
        np_flag, np_dataloss, np_uvw = extract_flags_dataloss_numpy(input_path)
        np_flag, _, _ = resample_data(time_resolution, time_axis, np_flag)
        np_dataloss, _, _ = resample_data(time_resolution, time_axis, np_dataloss)
        np_uvw, *_ = resample_uvw(time_resolution, time_axis, np_uvw)

        # They should be equal
        self.assertTrue(numpy.array_equal(np_flag, flag))
        self.assertTrue(numpy.array_equal(np_dataloss, dataloss))
        self.assertTrue(numpy.array_equal(np_uvw, uvw))

        inspect_ds.close()

    @unittest.skipUnless(is_input_data_available(), "Missing test data")
    def test_lazy_column_equal_numpy(self):
        for index, ms_path in enumerate(self.ms_list):
            self._test_ms(index, ms_path)

    def _test_ms_flags_resample(self, index, ms_path):

        time_resolution = 60
        input_path = ms_path

        output_path = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
        inspect_ds = h5py.File(output_path, 'w')
        inspect_ds.attrs['VERSION'] = INSPECT_DS_VERSION
        inspect_ds.create_group('DATASET')
        dataset = inspect_ds['/DATASET']
        dataset.attrs.update(derive_observation_metadata(input_path))
        dataset.attrs.update(derive_pointing(input_path))
        dataset.attrs.update(derive_reference_frame(input_path))
        dataset['FREQUENCIES'] = get_channel_frequencies(input_path)
        dataset['BASELINES'] = get_baselines(input_path)

        time_axis = get_time_samples(input_path)
        flag, _, _ = extract_flags_dataloss(dataset, input_path)

        resampled_flag, resampled_time_axis, total_counts = resample_data(
            time_resolution, time_axis, flag)

        self.assertEqual(flag.numpy_array.sum(), resampled_flag.sum(),
                         'The total should be preserved')
        self.assertEqual(flag.n_times, total_counts.sum(),
                         'The total number of items be preserved')

    @unittest.skipUnless(is_input_data_available(), "Missing test data")
    def test_resampling_of_flags(self):
        for index, ms_path in enumerate(self.ms_list):
            self._test_ms_flags_resample(index, ms_path)
