import numpy
import datetime
from unittest import TestCase
from unittest.mock import patch
import pytest

from lofar_quality.inspect.common import antennas_that_where_not_available_at_date, \
    LOFAR_ALL_ANTENNAS, LOFAR_INTERNATIONAL_ANTENNAS, LOFAR_CORE_ANTENNAS
from lofar_quality.inspect.v2 import \
    fix_antenna_to_avoid_underscoring_unavailable_at_timestamp
from lofar_quality.inspect import v2


class TestInspect(TestCase):

    def setUp(self) -> None:
        self.test_timestamp = datetime.datetime(2013, 1, 1, 0, 0, 0)
        expected_set = set(LOFAR_INTERNATIONAL_ANTENNAS)
        expected_set.add('RS210')
        self.expected_stations_at_timestamp = expected_set

    def test_stations_availability_listing(self):
        antennas = antennas_that_where_not_available_at_date(
            datetime.datetime(2001, 1, 1, 0, 0, 0))
        self.assertEqual(antennas, LOFAR_ALL_ANTENNAS)
        antennas = antennas_that_where_not_available_at_date(self.test_timestamp)
        self.assertSetEqual(antennas, self.expected_stations_at_timestamp)

    def test_fix_unavailable_antennas_core_only(self):

        antennas = list(LOFAR_CORE_ANTENNAS)
        fixed_antennas = fix_antenna_to_avoid_underscoring_unavailable_at_timestamp(
            antennas,
            self.test_timestamp)
        expected = set(list(LOFAR_CORE_ANTENNAS))

        self.assertSetEqual(set(fixed_antennas), expected)

    def test_fix_unavailable_antennas_dutch_only(self):

        antennas = list(LOFAR_CORE_ANTENNAS) + ['RS205']
        fixed_antennas = fix_antenna_to_avoid_underscoring_unavailable_at_timestamp(
            antennas,
            self.test_timestamp)
        expected = set(list(LOFAR_CORE_ANTENNAS) + ['RS205', 'RS210'])

        self.assertSetEqual(set(fixed_antennas), expected)

    def test_fix_unavailable_antennas_full(self):
        antennas = list(LOFAR_CORE_ANTENNAS) + ['PL601']
        fixed_antennas = fix_antenna_to_avoid_underscoring_unavailable_at_timestamp(
            antennas,
            self.test_timestamp)
        expected = set(list(self.expected_stations_at_timestamp) + antennas)

        self.assertSetEqual(set(fixed_antennas), expected)

    @patch('lofar_quality.inspect.v2.get_antenna_type', return_value="LBA")
    @patch('lofar_quality.inspect.v2.compute_pointing_metrics', return_value=(
        dict(
            elevation_fraction = 0.49,
            Sun = 19.9,
            Jupiter = 14.9,
            Moon = 14.9,
        ),
        # We are only interested in the first return type, add None for the others
        None,
        None,
        None,
    ))
    def test_elevation_metrics_LBA_poor(self, antenna_type_mock, compute_pointing_metrics_mock):
        scores, pointing_metrics = v2.elevation_metrics("fake_dataset_path")
        assert all(quality.value == "poor" for quality in scores.values())

    @patch('lofar_quality.inspect.v2.get_antenna_type', return_value="LBA")
    @patch('lofar_quality.inspect.v2.compute_pointing_metrics', return_value=(
        dict(
            elevation_fraction = 0.49,
            Sun = 19.9,
            Jupiter = 4.9,
            Moon = 9.9,
        ),
        # We are only interested in the first return type, add None for the others
        None,
        None,
        None,
    ))
    def test_elevation_metrics_HBA_poor(self, antenna_type_mock, compute_pointing_metrics_mock):
        scores, pointing_metrics = v2.elevation_metrics("fake_dataset_path")
        assert all(quality.value == "poor" for quality in scores.values())

    @patch('lofar_quality.inspect.v2.compute_pointing_metrics', return_value=(
        dict(
            elevation_fraction = 0,
            Sun = 25,
            Jupiter = 0,
            Moon = 0,
        ),
        # We are only interested in the first return type, add None for the others
        None,
        None,
        None,
    ))
    def test_elevation_metrics_sun_moderate(self, compute_pointing_metrics_mock):
        # Note that only the sun has a moderate value. The other targets are only good or poor
        for antenna_type in ["LBA", "HBA"]:
            with patch('lofar_quality.inspect.v2.get_antenna_type', return_value = antenna_type) as get_antenna_type_mock:
                scores, pointing_metrics = v2.elevation_metrics("fake_dataset_path", sun_limits=(20, 30))
            assert scores["sun_interference"].value == "moderate"

    @patch('lofar_quality.inspect.v2.get_antenna_type', return_value="LBA")
    @patch('lofar_quality.inspect.v2.compute_pointing_metrics', return_value=(
        dict(
            elevation_fraction = 0.51,
            Sun = 30.1,
            Jupiter = 15.1,
            Moon = 15.1,
        ),
        # We are only interested in the first return type, add None for the others
        None,
        None,
        None,
    ))
    def test_elevation_metrics_LBA_good(self, antenna_type_mock, compute_pointing_metrics_mock):
        scores, pointing_metrics = v2.elevation_metrics("fake_dataset_path")
        assert all(quality.value == "good" for quality in scores.values())


    @patch('lofar_quality.inspect.v2.get_antenna_type', return_value="HBA")
    @patch('lofar_quality.inspect.v2.compute_pointing_metrics', return_value=(
        dict(
            elevation_fraction = 0.51,
            Sun = 30.1,
            Jupiter = 5.1,
            Moon = 10.1,
        ),
        # We are only interested in the first return type, add None for the others
        None,
        None,
        None,
    ))
    def test_elevation_metrics_HBA_good(self, antenna_type_mock, compute_pointing_metrics_mock):
        scores, pointing_metrics = v2.elevation_metrics("fake_dataset_path")
        assert all(quality.value == "good" for quality in scores.values())

    def test_degree_incompleness(self):
        antenna_names = {'RS509', 'RS106', 'CS003', 'CS026', 'CS002', 'RS406', 'CS004', 'CS021', 'CS006', 'RS310', 'CS301', 'CS032', 'RS407', 'CS017', 'CS013', 'CS024', 'RS503', 'CS201', 'CS028', 'RS306', 'RS409', 'RS508', 'CS302', 'CS030', 'CS001', 'RS205', 'CS401', 'CS031', 'CS501', 'RS305', 'RS208', 'CS103', 'RS307', 'CS101', 'CS011', 'CS005', 'RS210', 'CS007'}
        assert not v2.degree_incompleteness(antenna_names)

        antenna_names.remove("RS509")
        antenna_names.remove("CS003")
        result = v2.degree_incompleteness(antenna_names)
        assert len(result) == 2
        assert "RS509" in result
        assert "CS003" in result

    def test_array_missing_important_remote_pair(self):
        antenna_names = {'RS509', 'RS106', 'CS003', 'CS026', 'CS002', 'RS406', 'CS004', 'CS021', 'CS006', 'RS310', 'CS301', 'CS032', 'RS407', 'CS017', 'CS013', 'CS024', 'RS503', 'CS201', 'CS028', 'RS306', 'RS409', 'RS508', 'CS302', 'CS030', 'CS001', 'RS205', 'CS401', 'CS031', 'CS501', 'RS305', 'RS208', 'CS103', 'RS307', 'CS101', 'CS011', 'CS005', 'RS210', 'CS007'}
        assert v2.array_missing_important_remote_pair(antenna_names).value == "good"

        important_stations = ("RS210", "RS310", "RS508", "RS509")
        for station in important_stations:
            antenna_names_incomplete = antenna_names.copy()
            antenna_names_incomplete.remove(station)
            assert v2.array_missing_important_remote_pair(antenna_names_incomplete).value == "poor"

    def test_array_missing_important_pairs_is(self):
        dutch_antenna_names = {'RS509', 'RS106', 'CS003', 'CS026', 'CS002', 'RS406', 'CS004', 'CS021', 'CS006', 'RS310', 'CS301', 'CS032', 'RS407', 'CS017', 'CS013', 'CS024', 'RS503', 'CS201', 'CS028', 'RS306', 'RS409', 'RS508', 'CS302', 'CS030', 'CS001', 'RS205', 'CS401', 'CS031', 'CS501', 'RS305', 'RS208', 'CS103', 'RS307', 'CS101', 'CS011', 'CS005', 'RS210', 'CS007'}
        international_antenna_names = {'DE601', 'DE605', 'PL610', 'PL611', 'PL612'}
        antenna_names = dutch_antenna_names.union(international_antenna_names)

        assert v2.array_missing_important_pairs_is(dutch_antenna_names) == "NA"
        assert v2.array_missing_important_pairs_is(antenna_names).value == "good"
        for station in international_antenna_names:
            antenna_names_incomplete = antenna_names.copy()
            antenna_names_incomplete.remove(station)
            assert v2.array_missing_important_pairs_is(antenna_names_incomplete).value == "poor"

    def test_array_high_data_loss_on_is_important_pair(self):
        dutch_antenna_names = {'RS509', 'RS106', 'CS003', 'CS026', 'CS002', 'RS406', 'CS004', 'CS021', 'CS006', 'RS310', 'CS301', 'CS032', 'RS407', 'CS017', 'CS013', 'CS024', 'RS503', 'CS201', 'CS028', 'RS306', 'RS409', 'RS508', 'CS302', 'CS030', 'CS001', 'RS205', 'CS401', 'CS031', 'CS501', 'RS305', 'RS208', 'CS103', 'RS307', 'CS101', 'CS011', 'CS005', 'RS210', 'CS007'}
        international_antenna_names = {'DE601', 'DE605', 'PL610', 'PL611', 'PL612'}

        dataloss = {name: 0 for name in dutch_antenna_names}

        # Verify NA if contains no international stations
        result = v2.array_high_data_loss_on_is_important_pair(dataloss.keys(), dataloss.values())
        assert result == "NA"

        for antenna in international_antenna_names:
            dataloss[antenna] = 24.9

        # Verify good quality if all of the important antennae have <25% dataloss
        result = v2.array_high_data_loss_on_is_important_pair(dataloss.keys(), dataloss.values())
        assert result.value == "good"

        # Verify moderate quality if one of the important german antennae has >25% dataloss
        for antenna in international_antenna_names:
            dataloss_temp = dataloss.copy()
            dataloss_temp[antenna] = 25.1
            result = v2.array_high_data_loss_on_is_important_pair(dataloss_temp.keys(), dataloss_temp.values())
            assert result.value == "moderate"


    def test_array_high_data_loss_on_dutch_important_pair(self):
        dutch_antenna_names = {'RS509', 'RS106', 'CS003', 'CS026', 'CS002', 'RS406', 'CS004', 'CS021', 'CS006', 'RS310', 'CS301', 'CS032', 'RS407', 'CS017', 'CS013', 'CS024', 'RS503', 'CS201', 'CS028', 'RS306', 'RS409', 'RS508', 'CS302', 'CS030', 'CS001', 'RS205', 'CS401', 'CS031', 'CS501', 'RS305', 'RS208', 'CS103', 'RS307', 'CS101', 'CS011', 'CS005', 'RS210', 'CS007'}
        dataloss = {name: 0 for name in dutch_antenna_names}
        for antenna in ['RS210', 'RS310', 'RS508', 'RS509']:
            dataloss[antenna] = 24.9

        # Verify good quality if all of the important antennae have <25% dataloss
        result = v2.array_high_data_loss_on_dutch_important_pair(dataloss.keys(), dataloss.values())
        assert result.value == "good"

        # Verify moderate quality if one of the important antennae has >25% dataloss
        for antenna in ['RS210', 'RS310', 'RS508', 'RS509']:
            dataloss[antenna] = 25.1
            result = v2.array_high_data_loss_on_dutch_important_pair(dataloss.keys(), dataloss.values())
            assert result.value == "moderate"

        # Verify NA if contains international stations but not any of the important dutch stations
        dataloss['DE605'] = 0
        for antenna in ['RS210', 'RS310', 'RS508', 'RS509']:
            dataloss.pop(antenna)
        result = v2.array_high_data_loss_on_dutch_important_pair(dataloss.keys(), dataloss.values())
        assert result == "NA"

@pytest.mark.parametrize("dataloss_per_antenna, expected_quality", [
    (numpy.array(3 * [4.9]), "good"),
    (numpy.array(3 * [5.1]), "moderate"),
    (numpy.array(3 * [9.9]), "moderate"),
    (numpy.array(3 * [10.1]), "poor"),
    (None, "unknown"),
])
def test_aggregated_array_data_losses_percentage(dataloss_per_antenna, expected_quality):
    result = v2.aggregated_array_data_losses_percentage(dataloss_per_antenna)
    assert result.value == expected_quality

@pytest.mark.parametrize("mean_rfi, antenna_type, expected_quality", [
    (1.99, "HBA", "good"),
    (2.01, "HBA", "moderate"),
    (3.49, "HBA", "moderate"),
    (3.51, "HBA", "poor"),
    (1.74, "LBA", "good"),
    (1.76, "LBA", "moderate"),
    (2.99, "LBA", "moderate"),
    (3.01, "LBA", "poor"),
])
def test_is_high_rfi(mean_rfi, antenna_type, expected_quality):
    class FakeMS():
        """Class to fake a measurement set with the desired attributes"""

        @property
        def attrs(self):
            return dict(
                RFI_PERCENTAGE = numpy.array(3*[mean_rfi]),
                antenna_filter = antenna_type,
            )

        def __getitem__(self, attr_name):
            return self.attrs[attr_name]

    fake_dataset = FakeMS()
    scoring = v2.is_high_rfi(fake_dataset)
    assert scoring.value == expected_quality
