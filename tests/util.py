from glob import glob
import os

TEST_DATA_DIRECTORY = os.path.join(os.path.dirname(__file__), 'data')


def list_test_data_ms():
    return glob(os.path.join(TEST_DATA_DIRECTORY, '*.MS'))

def is_input_data_available():
    return len(list_test_data_ms()) > 1
