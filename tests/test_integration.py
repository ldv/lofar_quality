import os
import tempfile
import unittest
from glob import glob

from lofar_quality.create.v1 import create_inspect_ds as create_inspect_ds_v1
from lofar_quality.create.v2 import create_inspect_ds as create_inspect_ds_v2
from lofar_quality.inspect.inspect_dataset import extract_quality_metrics, \
    plot_inspect_ds
from lofar_quality.manipulate.common import join_inspect_ds
from tests.util import list_test_data_ms, is_input_data_available


class TestIntegration(unittest.TestCase):
    def setUp(self) -> None:
        self.ms_list = list_test_data_ms()
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.tmpdir_path = self.tmp_dir.name

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_create_inspect_dataset_v1(self):

        ms_path, *_ = self.ms_list
        output_inspect_ds = os.path.join(self.tmpdir_path, 'inspect_ds.h5')
        create_inspect_ds_v1(ms_path, output_inspect_ds)
        self.assertTrue(os.path.exists(output_inspect_ds),
                        'Output inspect dataset not created')

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_create_inspect_dataset_v2(self):

        ms_path, *_ = self.ms_list
        output_inspect_ds = os.path.join(self.tmpdir_path, 'inspect_ds.h5')
        create_inspect_ds_v2(ms_path, output_inspect_ds)
        self.assertTrue(os.path.exists(output_inspect_ds),
                        'Output inspect dataset not created')

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_join_datasets_v1(self):
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds_v1(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)

        self.assertTrue(os.path.exists(output_ds_path),
                        'Joined inspect dataset not created')

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_join_datasets_v2(self):
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds_v2(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)

        self.assertTrue(os.path.exists(output_ds_path),
                        'Joined inspect dataset not created')

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_create_quality_metrics_dataset(self):
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        output_inspect_path = os.path.join(self.tmpdir_path, 'output.json')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds_v1(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)

        extract_quality_metrics(output_ds_path, output_inspect_path,
                                0.5, 0.3, 0.4, 0.5)

        self.assertTrue(os.path.exists(output_inspect_path))

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_create_quality_metrics_dataset_v2(self):
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        output_inspect_path = os.path.join(self.tmpdir_path, 'output.json')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds_v2(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)

        extract_quality_metrics(output_ds_path, output_inspect_path,
                                0.5, 0.3, 0.4, 0.5)

        self.assertTrue(os.path.exists(output_inspect_path))

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_create_plots_v1(self):
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        output_inspect_path = os.path.join(self.tmpdir_path, 'output')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds_v1(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)

        plot_inspect_ds(output_ds_path, output_inspect_path, 0.3)

        self.assertTrue(os.path.exists(output_inspect_path))
        output_plots = glob(os.path.join(output_inspect_path, '*.png'))
        self.assertEqual(len(output_plots), 6)

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_create_plots_v2(self):
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        output_inspect_path = os.path.join(self.tmpdir_path, 'output')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds_v2(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)
        plot_inspect_ds(output_ds_path, output_inspect_path, .3)
        self.assertTrue(os.path.exists(output_inspect_path))
        output_plots = glob(os.path.join(output_inspect_path, '*.png'))
        self.assertEqual(len(output_plots), 4)


if __name__ == '__main__':
    unittest.main()
