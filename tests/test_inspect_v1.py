import os
import tempfile
import unittest

import h5py

from lofar_quality.create.v1 import create_inspect_ds
from lofar_quality.inspect.common import LOFAR_DUTCH_ANTENNAS
from lofar_quality.inspect.common import count_international_stations
from lofar_quality.inspect.v1 import are_only_dutch, \
    transform_antenna_names_to_station_names, \
    missing_dutch_antennas, make_missing_data_per_frequency
from lofar_quality.manipulate.common import join_inspect_ds
from tests.util import is_input_data_available, list_test_data_ms


class TestCommonFunctions(unittest.TestCase):

    def test_is_dutch_array(self):
        antenna_names_false = ['CS0011', 'RS012', 'PL00121']
        antenna_names_true = ['CS0011', 'RS012']
        self.assertTrue(are_only_dutch(antenna_names_true))
        self.assertFalse(are_only_dutch(antenna_names_false))

    def test_transform_antenna_names(self):
        antenna_names = ['CS0011', 'RS012', 'PL00121HBA0']
        self.assertEqual({'CS0011', 'RS012', 'PL00121'},
            set(transform_antenna_names_to_station_names(antenna_names)))

    def test_missing_dutch_stations(self):
        station_names = ['CS011', 'RS406', 'PL610']
        result = missing_dutch_antennas(station_names)
        self.assertTrue(are_only_dutch(result), msg='not testing only dutch array')
        self.assertEqual(len(LOFAR_DUTCH_ANTENNAS) - 2, len(result))

    def test_n_international_stations(self):
        station_names = ['CS011', 'RS406', 'PL610']
        result = count_international_stations(station_names)
        self.assertEqual(result, 1)


class TestInspectDataset(unittest.TestCase):

    def setUp(self) -> None:
        self.ms_list = list_test_data_ms()
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.tmpdir_path = self.tmp_dir.name

        # Can this be broken down more?
        output_ds = []
        output_ds_path = os.path.join(self.tmpdir_path, 'inspect_full.h5')
        output_inspect_path = os.path.join(self.tmpdir_path, 'output')
        for index, ms_path in enumerate(self.ms_list):
            output_inspect_ds = os.path.join(self.tmpdir_path, f'inspect_{index}.h5')
            create_inspect_ds(ms_path, output_inspect_ds)
            output_ds.append(output_inspect_ds)
        join_inspect_ds(output_ds, output_ds_path)

        self.dataset = h5py.File(output_ds_path)['/DATASET']

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()

    @unittest.skipUnless(is_input_data_available(), 'Missing test data')
    def test_make_missing_data_per_frequency(self):
        # TODO: not an optimal test; not clear exactly what the goal is of the method
        # or the prerequisites
        n_frequencies = len(self.dataset['FREQUENCIES'][:])

        frequencies, data_loss_per_frequency, flagging_per_frequency = make_missing_data_per_frequency(
            self.dataset)

        # What are the operations doing? What should be tested? Dimensions? Averages?
        self.assertEqual(n_frequencies, len(frequencies), "Frequencies does not match")
        self.assertEqual(n_frequencies, len(data_loss_per_frequency),
                         "Data loss does not match")
        self.assertEqual(n_frequencies, len(flagging_per_frequency),
                         "Flagging does not match")


if __name__ == '__main__':
    unittest.main()
