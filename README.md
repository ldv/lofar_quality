# LOFAR Quality

LOFAR quality is a set of command line tools that can be used interactively or in pipelines
to extract the quality metrics of an interferometric LOFAR observation.

To facilitate the manipulation of the data and reduce the amount of data moved around. This project uses a ancillary
data format to the MS to store the flagging, missing data information and the observation schedule data.

## Install the application
To use the application you can either use docker by running the following command
```
docker run --rm -it git.astron.nl:5000/ldv/lofar_quality:latest /bin/bash
```

or run it in singularity container with the command
```
singularity shell docker://git.astron.nl:5000/ldv/lofar_quality:latest
```

or install it locally by cloning this repository and running
```
python setup.py install
```
or 
```
python3 setup.py install
```
depending on your python installation.

## Create an Inspect-dataset
To create and inspect dataset you can run the following command
```
inspect_utils.py create [measurement set path] [inspect dataset path]
```

## To combine two or more Inspect-datasets
To join two or more inspect dataset it is possible by executing the following command
```
inspect_utils.py join  [list of inspect dataset of input] [output inspect dataset path]
```
ex.
```
inspect_utils.py join dataset_sb001.h5 dataset_sb002.h5 dataset.h5
```



