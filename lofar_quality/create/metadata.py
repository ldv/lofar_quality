import numpy
from casacore.tables import table as ms_table, taql
from pathlib import Path

def parse_sas_id_from_ms_filename(ms_path):
    """Extract the sas-ID from a file name.
    Expected name format: L012345_SB012_uv.MS or similar.
    The main requirement is that one part seperated by underscores starts with an 'L',
    followed by an integer representing the SAS-ID.
    """
    basename = Path(ms_path).name
    for name_slice in basename.split("_"):
        if name_slice.startswith("L"):
            sas_id = name_slice[1:]
            # After removing the leading 'L' we should be left with only a number
            try:
                int(sas_id) # check if sas_id is an integer, else continue checking sections of the filename
                return sas_id
            except ValueError:
                continue
    raise ValueError(f"Unable to find sas-id in file: {ms_path}")

def derive_observation_metadata(ms_path):
    data_table = ms_table(ms_path)
    observation_table = ms_table(f'{ms_path}::OBSERVATION')
    antenna_table = ms_table(f'{ms_path}::ANTENNA')
    spec_window_table = ms_table(f'{ms_path}::SPECTRAL_WINDOW')
    polarization_table = ms_table(f'{ms_path}::POLARIZATION')
    obsid = parse_sas_id_from_ms_filename(ms_path)
    start_time = observation_table.getcell('LOFAR_OBSERVATION_START', 0)
    end_time = observation_table.getcell('LOFAR_OBSERVATION_END', 0)
    max_frequency = observation_table.getcell('LOFAR_OBSERVATION_FREQUENCY_MAX', 0)
    min_frequency = observation_table.getcell('LOFAR_OBSERVATION_FREQUENCY_MIN', 0)
    center_frequency = observation_table.getcell('LOFAR_OBSERVATION_FREQUENCY_CENTER',
                                                 0)
    n_polarizations = polarization_table.getcell('NUM_CORR', 0)
    nchannels = spec_window_table.getcell('NUM_CHAN', 0)
    antenna_set = observation_table.getcell('LOFAR_ANTENNA_SET', 0)
    clock = observation_table.getcell('LOFAR_CLOCK_FREQUENCY', 0)
    subband_name = spec_window_table.getcell('NAME', 0)
    antenna_names = list(antenna_table.getcol('NAME'))
    filter_selection = observation_table.getcell('LOFAR_FILTER_SELECTION', 0)
    n_baselines = len(antenna_names) * (len(antenna_names) + 1) // 2
    n_rows = data_table.nrows()
    n_times = n_rows // n_baselines
    return dict(obs_id=obsid,
                target=observation_table.getcell('LOFAR_TARGET', 0),
                project=observation_table.getcell('PROJECT', 0),
                obs_start_time=start_time,
                obs_end_time=end_time,
                obs_max_frequency=max_frequency, obs_min_frequency=min_frequency,
                obs_center_frequency=center_frequency,
                antenna_names=antenna_names, antenna_set=antenna_set, clock=clock,
                antenna_filter=filter_selection, subbands=[subband_name],
                num_channels=nchannels,
                num_polarizations=n_polarizations,
                num_antennas=len(antenna_names),
                num_baselines=n_baselines,
                num_times=n_times)


def derive_pointing(ms_path):
    field_table = ms_table(f'{ms_path}::FIELD')
    return dict(phase_direction=list(field_table.getcol('PHASE_DIR')[0, 0, :]),
                reference_direction=list(field_table.getcol('REFERENCE_DIR')[0, 0, :]),
                tile_beam_direction=list(
                    field_table.getcol('LOFAR_TILE_BEAM_DIR')[0, :]))


def derive_reference_frame(ms_path):
    antenna_table = ms_table(f'{ms_path}::ANTENNA')
    field_position = numpy.mean(antenna_table.getcol('POSITION'), axis=0)
    return dict(reference_station_position=list(field_position))


def get_channel_frequencies(ms_path):
    spec_window = ms_table(f'{ms_path}::SPECTRAL_WINDOW')
    return numpy.array(spec_window.getcell('CHAN_FREQ', 0))


def get_time_samples(ms_path):
    main = ms_table(f'{ms_path}')
    unique_times = taql('SELECT TIME FROM $1 ORDERBY DISTINCT TIME',
                        tables=[main]).getcol('TIME')
    return unique_times


def get_baselines(ms_path):
    antenna_table = ms_table(f'{ms_path}::ANTENNA')
    n_antennas = antenna_table.nrows()
    n_baselines = int((n_antennas + 1) * n_antennas * .5)
    baselines = numpy.zeros((n_baselines), dtype='S20')
    k = 0
    for i in range(n_antennas):
        for j in range(i + 1):
            ant1 = antenna_table.getcell('NAME', i)
            ant2 = antenna_table.getcell('NAME', j)
            baselines[k] = f'{ant1}-{ant2}'
            k += 1
    return baselines