import tempfile
from typing import Tuple, Union

import h5py
import numpy
from casacore.tables import table as ms_table
from numpy import ndarray

from lofar_quality.common import LEGACY_INSPECT_DS_VERSION
from .metadata import derive_observation_metadata, derive_pointing, \
    derive_reference_frame, get_channel_frequencies, get_baselines, get_time_samples


class LazyColumn:
    def __init__(self, table: ms_table, column_name, shape, preload_operation=None):
        """
        Parameters:
             table (ms_table): Input MeasurementSet table
             column_name (str): name of the column to be read
             shape (list[int]|tuple[int | Any, int, Any, int]): dimensions
             preload_operation func(float)->float: operation to execute on read
        Return:
            LazyColumn
        """
        self.shape = shape
        self.n_times = shape[0]
        self.n_baselines = shape[1]
        self.n_coordinates = 0
        self.n_polarization = 0
        self.n_channels = 0
        self.is_uvw = False
        if len(shape) > 3:
            self.n_channels = shape[2]
            self.n_polarization = shape[3]
        else:
            self.is_uvw = True
            self.n_coordinates = shape[2]

        self.column_name = column_name
        self._table = table
        self.preload_operation = preload_operation

    def __getitem__(self, item):
        shape_from_slice = [
            dim.stop - dim.start if (dim.stop != dim.start) and (dim.stop != 0) else
            self.shape[i] for
            i, dim in
            enumerate(item)]
        start_idx = item[0].start * self.n_baselines
        end_idx = item[0].stop * self.n_baselines
        if self.preload_operation:
            return self.preload_operation(
                self._table.getcol(self.column_name, startrow=start_idx,
                                   nrow=end_idx - start_idx).reshape(
                    shape_from_slice, order='C'))
        else:
            return self._table.getcol(self.column_name, startrow=start_idx,
                                      nrow=end_idx - start_idx).reshape(
                shape_from_slice, order='C')

    @property
    def numpy_array(self):
        result = self._table.getcol(self.column_name)
        if self.preload_operation:
            result = self.preload_operation(result)
        return result


def open_column_as_memmapped_array(table: ms_table, column, shape, dtype=bool,
                                   load_operation=None):
    _, filename = tempfile.mkstemp()
    memmapped_array = numpy.memmap(filename, dtype=type, mode='w+', shape=shape,
                                   order='C')
    table.getcolnp(column, memmapped_array)
    return memmapped_array


def extract_flags_dataloss(dataset, ms_path):
    n_antennas = ms_table(f'{ms_path}::ANTENNA').nrows()
    n_channels = dataset['FREQUENCIES'].shape[0]
    main_table = ms_table(ms_path)
    n_baselines = int((n_antennas + 1) * n_antennas * .5)
    n_rows = main_table.nrows()
    n_polarizations = 4

    n_times = n_rows // n_baselines
    flags = LazyColumn(main_table, 'FLAG',
                       (n_times, n_baselines, n_channels, n_polarizations))
    uvw = LazyColumn(main_table, 'UVW', (n_times, n_baselines, 3))
    dataloss = LazyColumn(main_table, 'DATA',
                          shape=(n_times, n_baselines, n_channels, n_polarizations),
                          preload_operation=lambda x: x == 0)

    return flags, dataloss, uvw


def resample_data(time_resolution: float, time_axis: numpy.ndarray, data: LazyColumn) -> \
        Tuple[
            ndarray, ndarray, ndarray]:
    """ Resample data over time to reduce the amount of generated flag data for the quality plots """
    original_resolution = time_axis[1] - time_axis[0]

    sampling_size = int(time_resolution / original_resolution)
    # data.shape[0] is time dimensions
    n_iteration = int(data.shape[0] // sampling_size)
    # account for the final bucket (which might be smaller than time_resolution)
    if n_iteration * sampling_size < time_axis.shape[0]:
        n_iteration = n_iteration + 1
    resampled_data = numpy.zeros((n_iteration, *data.shape[1:]), dtype=numpy.ushort)
    time_axis_resample = numpy.zeros((n_iteration,), dtype=time_axis.dtype)
    total_counts = numpy.zeros((n_iteration,), dtype=numpy.ushort)

    for k in range(n_iteration):
        start_sample, end_sample = k * sampling_size, min((k + 1) * sampling_size,
                                                          data.shape[0])
        total_counts[k] = end_sample - start_sample
        if len(data.shape) == 3:
            resampled_data[k, :, :] = numpy.sum(data[start_sample: end_sample, :, :],
                                                axis=0)
        if len(data.shape) == 4:
            resampled_data[k, :, :, :] = numpy.sum(
                data[start_sample: end_sample, :, :, :], axis=0)
        time_axis_resample[k] = time_axis[k * sampling_size]
    return resampled_data, time_axis_resample, total_counts


def resample_uvw(time_resolution: float, time_axis: numpy.ndarray,
                 uvw: Union[numpy.ndarray, LazyColumn]) -> Tuple[
    ndarray, ndarray, ndarray]:
    original_resolution = time_axis[1] - time_axis[0]

    sampling_size = int(time_resolution / original_resolution)
    # data.shape[0] is time

    n_iteration = int(uvw.shape[0] // sampling_size)
    # account for the final bucket (which might be smaller than time_resolution)
    if n_iteration * sampling_size < time_axis.shape[0]:
        n_iteration = n_iteration + 1

    resampled_data = numpy.zeros((n_iteration, *uvw.shape[1:]), dtype=numpy.float32)
    time_axis_resample = numpy.zeros((n_iteration,), dtype=time_axis.dtype)
    total_counts = numpy.zeros((n_iteration,), dtype=numpy.ushort)

    for k in range(n_iteration):
        start_sample, end_sample = k * sampling_size, min((k + 1) * sampling_size,
                                                          uvw.shape[0])
        total_counts[k] = end_sample - start_sample
        resampled_data[k, :, :] = numpy.mean(uvw[start_sample: end_sample, :, :],
                                             axis=0)
        time_axis_resample[k] = time_axis[k * sampling_size]
    return resampled_data, time_axis_resample, total_counts


def create_inspect_ds(input_path: str, output_path: str, time_resolution: int = 60):
    inspect_ds = h5py.File(output_path, 'w')
    inspect_ds.attrs['VERSION'] = LEGACY_INSPECT_DS_VERSION
    inspect_ds.create_group('DATASET')
    dataset = inspect_ds['/DATASET']
    dataset.attrs.update(derive_observation_metadata(input_path))
    dataset.attrs.update(derive_pointing(input_path))
    dataset.attrs.update(derive_reference_frame(input_path))
    dataset['FREQUENCIES'] = get_channel_frequencies(input_path)
    dataset['BASELINES'] = get_baselines(input_path)
    flag, dataloss, uvw = extract_flags_dataloss(dataset, input_path)
    time_axis = get_time_samples(input_path)
    flag, _, _ = resample_data(time_resolution, time_axis, flag)
    dataloss, resample_time_axis, total_counts = resample_data(time_resolution,
                                                               time_axis, dataloss)
    uvw, *_ = resample_uvw(time_resolution, time_axis, uvw)

    dataset['FLAG'], dataset['DATALOSS'], dataset['UVW'] = flag, dataloss, uvw
    dataset['COUNTS_PER_SAMPLE'] = total_counts

    dataset['TIME'] = resample_time_axis

    inspect_ds.close()
