import logging
import subprocess
import warnings

import h5py
import numpy as np

from lofar_quality.common import INSPECT_DS_VERSION
from .metadata import *


class CMDException(Exception):
    def __init__(self, cmd, sub_process_return: subprocess.CompletedProcess):
        self.cmd = cmd
        self.sub_process_return = sub_process_return

        super().__init__(
            f"Error executing command ({cmd}): {sub_process_return.returncode} - {sub_process_return.stderr}")


def parse_aoquality_output(stdout):
    return np.loadtxt(stdout.split('\n'), delimiter='\t', skiprows=1, dtype=float)


def run_and_return_stdout_stderr(cmd):
    result: subprocess.CompletedProcess = subprocess.run(cmd, stdout=subprocess.PIPE,
                                                         stderr=subprocess.PIPE)
    if result.returncode != 0:
        raise CMDException(cmd, result)
    else:
        return result.stdout.decode(), result.stderr.decode()


def run_and_parse_aoquality(cmd):
    stdout, _ = run_and_return_stdout_stderr(cmd)
    return parse_aoquality_output(stdout)


def get_antd_stdev_stats(in_ms):
    """
    Get the queried count stats of an input MS and eventually append it to an
     output file.
    function to query the antennae DStandardDeviation statistics given a MS in input and
    eventually provide a csv file with added antennae names as output
    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary csv file

    Returns
    -------
    result : Tuple[np.ndarray, np.ndarray]
        Antenna names, Differential standard deviations.
    """
    try:
        # read counts per antenna and store in an array
        parsed_output = run_and_parse_aoquality((
            'aoquality',
            'query_a',
            'DStandardDeviation',
            in_ms
        ))
        return parsed_output[:, 1:]
    except CMDException as e:
        logging.warning(f"Cannot read DStandardDeviation from metadata. Error: {e}")
        return numpy.nan

def get_ant_dcount_stats(in_ms):
    """
    Get the queried count stats of an input MS and eventually append it to an
     output file.
    function to query the antennae DCount statistics given a MS in input and
    eventually provide a csv file with added antennae names as output
    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary csv file

    Returns
    -------
    result : Tuple[np.ndarray, np.ndarray]
        Antenna names, Differential standard deviations.
    """
    try:
        # read counts per antenna and store in an array
        parsed_output = run_and_parse_aoquality((
            'aoquality',
            'query_a',
            'DCount',
            in_ms
        ))
        result = parsed_output[:, (1, 3, 5, 7)]
    except CMDException as e:
        logging.warning(f"Cannot read DCount from metadata. Error: {e}")
        result = numpy.nan
    return result

def get_ant_dsum_stats(in_ms):
    """
    Get the queried count stats of an input MS and eventually append it to an
     output file.
    function to query the antennae DSum statistics given a MS in input and
    eventually provide a csv file with added antennae names as output
    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary csv file

    Returns
    -------
    result : Tuple[np.ndarray, np.ndarray]
        Antenna names, Differential standard deviations.
    """
    # read counts per antenna and store in an array
    try:
        parsed_output = run_and_parse_aoquality((
            'aoquality',
            'query_a',
            'DSum',
            in_ms
        ))
        result = parsed_output[:, 1:]
    except CMDException as e:
        logging.warning(f"Cannot read DSum from metadata. Error: {e}")
        result = numpy.nan
    return result

def get_ant_dsump2_stats(in_ms):
    """
    Get the queried count stats of an input MS and eventually append it to an
     output file.
    function to query the antennae DSumP2 statistics given a MS in input and
    eventually provide a csv file with added antennae names as output
    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary csv file

    Returns
    -------
    result : Tuple[np.ndarray, np.ndarray]
        Antenna names, Differential standard deviations.
    """
    # read counts per antenna and store in an array
    try:
        parsed_output = run_and_parse_aoquality((
            'aoquality',
            'query_a',
            'DSumP2',
            in_ms
        ))
        result = parsed_output[:, 1:]
    except CMDException as e:
        logging.warning(f"Cannot read DSumP2 from metadata. Error: {e}")
        result = numpy.nan
    return result

def get_ant_count_stats(in_ms):
    """
    Get the queried count stats of an input MS and eventually append it to a output file.

    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary csv file

    Returns
    -------
    result : arrays, file
        Antenna names, Nr sample counts and Output (file)name.
    """
    # read counts per antenna and store in an array
    try:
        antCounts = run_and_parse_aoquality(('aoquality', 'query_a', 'Count', in_ms))[:, 1]
    except CMDException as e:
        logging.warning(f"Cannot read Count from metadata. Error: {e}")
        antCounts = numpy.nan
    return antCounts


def get_time_count_stats(in_ms):
    """
    Get the queried count stats of an input MS per time

    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary csv file

    Returns
    -------
    result : arrays, file
        Antenna names, Nr sample counts and Output (file)name.
    """
    try:
        count_per_time = run_and_parse_aoquality(('aoquality', 'query_t', 'Count', in_ms))
        return count_per_time[:, 0], count_per_time[:, 1]
    except CMDException as e:
        logging.warning(f"Cannot read Count from metadata. Error: {e}")
        return numpy.nan, numpy.nan


def get_ant_rfi_perc_stats(in_ms):
    """
    Get the queried rfipercentage stats of an input MS and eventually append it to a output file.

    Parameters
    ----------
    in_ms : str
        The full path to the MS file
    in_debug: bool, optional
        Print auxiliary info

    Returns
    -------
    result : arrays, file
        Frequencies (of channels), RFI percentage and Output (file)name.
    """
    try:
        rfiperc = run_and_parse_aoquality(('aoquality',
                                       'query_f',
                                       'RFIPercentage',
                                       in_ms))
        return rfiperc[:, 0], rfiperc[:, 1]
    except CMDException as e:
        logging.warning(f"Cannot read RFIPercentage from metadata. Error: {e}")
        return numpy.array([]), numpy.nan


def trim_border_frequencies(frequencies, rfi_percentage):
    """
    The first two and the last two channels of an observation subband
    are always flagged so there is no point in considering them.
    For this reason this method removes them.
    """
    rfi_percentage[0:2] = np.nan
    rfi_percentage[-2:0] = np.nan
    return frequencies, rfi_percentage


def create_inspect_ds(input_path: str, output_path: str, **kwargs):
    inspect_ds = h5py.File(output_path, 'w')
    inspect_ds.attrs['VERSION'] = INSPECT_DS_VERSION
    inspect_ds.create_group('DATASET')
    dataset = inspect_ds['/DATASET']
    dataset.attrs.update(derive_observation_metadata(input_path))
    dataset.attrs.update(derive_pointing(input_path))
    dataset.attrs.update(derive_reference_frame(input_path))

    dataset['COUNT_PER_TIME'] = get_time_count_stats(input_path)
    dataset['TIME'] = numpy.linspace(dataset.attrs['obs_start_time'],
                                     dataset.attrs['obs_end_time'],
                                     dataset.attrs["num_times"])
    dataset.attrs['num_time_samples'] = dataset.attrs["num_times"]
    dataset['VISIBILITY_COUNTS'] = get_ant_count_stats(input_path)

    frequencies, rfi_percentage = get_ant_rfi_perc_stats(input_path)
    if frequencies.size != 0:
        dataset.attrs['num_frequency_samples'] = frequencies.shape[0]
        dataset['FREQUENCIES'], dataset['RFI_PERCENTAGE'] = trim_border_frequencies(
            frequencies, rfi_percentage
        )
    else:
        dataset.attrs['num_frequency_samples'] = 0
        dataset['FREQUENCIES'] = frequencies
        dataset['RFI_PERCENTAGE'] = rfi_percentage

    dataset['VISIBILITY_STD'] = get_antd_stdev_stats(input_path)
    
    dataset['DSum'] = get_ant_dsum_stats(input_path)
    dataset['DCount'] = get_ant_dcount_stats(input_path)
    dataset['DSumP2'] = get_ant_dsump2_stats(input_path)

    inspect_ds.close()
