import datetime
import enum
import itertools
import json
from typing import List, Dict, Union, Set

import astropy.units as u
import h5py
import matplotlib
import matplotlib.pyplot as plt
import numpy
import re
import logging
from astropy.coordinates import EarthLocation, SkyCoord, AltAz, get_body
from astropy.time import Time
from matplotlib.dates import DateFormatter


class QualityScore(enum.Enum):
    GOOD = "good"
    MODERATE = "moderate"
    POOR = "poor"
    UNKNOWN = "unknown"


matplotlib.use('Agg')

TARGETS = [{'name': 'CasA', 'ra': 6.123487680622104, 'dec': 1.0265153995604648},
           {'name': 'CygA', 'ra': 5.233686575770755, 'dec': 0.7109409582180791},
           {'name': 'TauA', 'ra': 1.4596748493730913, 'dec': 0.38422502335921294},
           {'name': 'HerA', 'ra': 4.4119087330382163, 'dec': 0.087135562905816893},
           {'name': 'VirA', 'ra': 3.276086511413598, 'dec': 0.21626589533567378},
           {'name': 'Sun'},
           {'name': 'Jupiter'},
           {'name': 'Moon'}]

LOFAR_ARRAY = [{'name': 'CS004',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS028',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS011',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS005',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS401',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS101',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS406',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS503',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS024',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS030',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS103',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS106',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS001',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS509',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS003',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS006',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS508',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS501',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS026',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS208',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS007',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS205',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS307',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS407',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS306',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS002',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS301',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'RS305',
                'type': 'R',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS032',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS302',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS201',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS031',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS017',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS021',
                'type': 'C',
                'since': datetime.datetime(2012, 11, 30, 23, 4, 37)},
               {'name': 'CS013',
                'type': 'C',
                'since': datetime.datetime(2012, 12, 6, 16, 4, 1)},
               {'name': 'RS310',
                'type': 'R',
                'since': datetime.datetime(2012, 12, 21, 21, 42, 3)},
               {'name': 'RS409',
                'type': 'R',
                'since': datetime.datetime(2012, 12, 21, 21, 42, 3)},
               {'name': 'DE603',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'SE607',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'DE605',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'DE604',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'DE601',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'UK608',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'DE602',
                'type': 'I',
                'since': datetime.datetime(2013, 2, 9, 17, 59)},
               {'name': 'FR606',
                'type': 'I',
                'since': datetime.datetime(2013, 3, 15, 18, 0)},
               {'name': 'RS210',
                'type': 'R',
                'since': datetime.datetime(2013, 7, 13, 11, 49, 22)},
               {'name': 'DE609',
                'type': 'I',
                'since': datetime.datetime(2015, 2, 4, 22, 0, 1)},
               {'name': 'PL611',
                'type': 'I',
                'since': datetime.datetime(2016, 2, 4, 19, 47, 59)},
               {'name': 'PL612',
                'type': 'I',
                'since': datetime.datetime(2016, 2, 4, 19, 47, 59)},
               {'name': 'PL610',
                'type': 'I',
                'since': datetime.datetime(2016, 2, 4, 19, 47, 59)},
               {'name': 'IE613',
                'type': 'I',
                'since': datetime.datetime(2017, 7, 25, 8, 20, 1)},
               {'name': 'LV614',
                'type': 'I',
                'since': datetime.datetime(2019, 9, 18, 4, 0, 1)}]

LOFAR_CORE_ANTENNAS = {x['name'] for x in
                       filter(lambda x: x['type'] == 'C', LOFAR_ARRAY)}
LOFAR_REMOTE_ANTENNAS = {x['name'] for x in
                         filter(lambda x: x['type'] == 'R', LOFAR_ARRAY)}
LOFAR_DUTCH_ANTENNAS = {x['name'] for x in
                        filter(lambda x: x['type'] in ['C', 'R'], LOFAR_ARRAY)}
LOFAR_INTERNATIONAL_ANTENNAS = {x['name'] for x in
                                filter(lambda x: x['type'] == 'I', LOFAR_ARRAY)}

LOFAR_ALL_ANTENNAS = {x['name'] for x in LOFAR_ARRAY}

IMPORTANT_REMOTE = {'RS210', 'RS310', 'RS508', 'RS509'}
IMPORTANT_INTERNATIONAL = {'DE601', 'DE605'}
IMPORTANT_INTERNATIONAL_MODERN = {'PL610', 'PL611', 'PL612'}


def antennas_that_where_not_available_at_date(timestamp: datetime.datetime) -> Set[str]:
    for idx, station_info in enumerate(LOFAR_ARRAY):
        if station_info['since'] > timestamp:
            return {station_info['name'] for station_info in LOFAR_ARRAY[idx:]}
    return set()


def transform_antenna_names_to_station_names(
        antenna_names: Union[List[str], Set[str]]) -> List[str]:
    pattern = r'^([a-zA-Z]+\d+)(?=[a-zA-Z]+\d*$|$)' # Strip trailing HBA0, HBA1, LBA from name
    def replace(name):
        match = re.match(pattern, name)
        if match is None:
            logging.warning(f"Unexpected naming pattern for station {name}")
            return ""
        return match.group(1)
    return [replace(name) for name in antenna_names]


def are_only_core(antenna_names: Union[List[str], Set[str]]) -> bool:
    return len(set(map(lambda x: x[:2], antenna_names)).difference({'CS'})) == 0


def missing_antennas(antenna_names: Union[List[str], Set[str]]) -> Set[str]:
    return LOFAR_ALL_ANTENNAS.difference(antenna_names)


def contains_remote(antenna_names: Union[List[str], Set[str]]) -> bool:
    for antenna in antenna_names:
        if antenna.startswith('RS'):
            return True
    return False


def are_only_dutch(antenna_names: Union[List[str], Set[str]]) -> bool:
    return len(set(map(lambda x: x[:2], antenna_names)).difference({'CS', 'RS'})) == 0


def count_international_stations(antenna_names: Union[List[str], Set[str]]) -> int:
    station_names = transform_antenna_names_to_station_names(antenna_names)
    return len(set(station_names).difference(LOFAR_DUTCH_ANTENNAS))


def is_full_array(antenna_names: Union[List[str], Set[str]]) -> bool:
    return count_international_stations(antenna_names) > 0


def is_modern_array(antenna_names: Union[List[str], Set[str]]) -> bool:
    return len(IMPORTANT_INTERNATIONAL_MODERN.difference(set(antenna_names))) > 0


def missing_dutch_antennas(antenna_names: Union[List[str], Set[str]]) -> Set[str]:
    return LOFAR_DUTCH_ANTENNAS.difference(
        LOFAR_DUTCH_ANTENNAS.intersection(
            transform_antenna_names_to_station_names(antenna_names)))


def missing_core_antennas(antenna_names: Union[List[str], Set[str]]) -> Set[str]:
    return LOFAR_CORE_ANTENNAS.difference(
        LOFAR_DUTCH_ANTENNAS.intersection(
            transform_antenna_names_to_station_names(antenna_names))
    )


def missing_international_antennas(antenna_names: Union[List[str], Set[str]]) -> \
        Set[str]:
    return LOFAR_INTERNATIONAL_ANTENNAS.difference(
        LOFAR_INTERNATIONAL_ANTENNAS.intersection(
            transform_antenna_names_to_station_names(antenna_names))
    )


def _separation_to_score(separation: float, limits: List[float]):
    """
    Returns a score based on the separation and the limit provided.
    """
    if separation > limits[1]:
        return 1
    elif limits[0] < separation <= limits[1]:
        return .5
    else:
        return 0


def derive_observing_frame(inspect_ds: h5py.Dataset) -> EarthLocation:
    field_position = inspect_ds['/DATASET'].attrs['reference_station_position']
    field_position: EarthLocation = EarthLocation.from_geocentric(x=field_position[0],
                                                                  y=field_position[1],
                                                                  z=field_position[2],
                                                                  unit='m')
    return field_position


def derive_pointing(inspect_ds: h5py.Dataset) -> SkyCoord:
    ra, dec = inspect_ds['/DATASET'].attrs['reference_direction']
    pointing: SkyCoord = SkyCoord(ra, dec, unit='rad', equinox='J2000')
    return pointing


def convert_mjs_to_time(mjs: float) -> Time:
    mjd = mjs / (3600 * 24)
    time = Time(mjd, format='mjd', scale='utc')
    return time


def derive_time_samples(inspect_ds: h5py.Dataset) -> Time:
    time = inspect_ds['/DATASET/TIME'][:]
    return convert_mjs_to_time(time)


def compute_altaz(observing_frame: EarthLocation, pointing: SkyCoord,
                  time: Time) -> float:
    """
    Compute the altitude of the source in degrees
    """
    pointing_in_altaz = pointing.transform_to(
        AltAz(location=observing_frame, obstime=time))
    return pointing_in_altaz.alt.deg


def _compute_separation(target, time: Time, pointing: SkyCoord,
                        observing_frame: EarthLocation) -> (float, float, str):
    """
    Compute the angular separation in degrees
    """
    target_name = target['name']
    if 'ra' not in target:
        coordinates_target = get_body(target_name, time)
    else:
        coordinates_target = SkyCoord(ra=target['ra'] * u.rad,
                                      dec=target['dec'] * u.rad)

    min_separation = numpy.min(coordinates_target.separation(pointing).deg)
    target_elevation = compute_altaz(observing_frame, coordinates_target, time)
    return target_elevation, min_separation, target_name


def compute_pointing_summary(time: Time, pointing: SkyCoord,
                             observing_frame: EarthLocation, min_elevation: float) -> \
        Dict[str, float]:
    pointing_elevation = compute_altaz(observing_frame, pointing, time)
    elevation_fraction = numpy.sum(pointing_elevation > min_elevation) / time.size
    pointing_summary = {'elevation_fraction': elevation_fraction}
    for target in TARGETS:
        _, min_separation, target_name = _compute_separation(target, time, pointing,
                                                             observing_frame)
        pointing_summary[target_name] = min_separation

    return pointing_summary


def compute_pointing_metrics(dataset: h5py.Dataset, min_elevation: float) -> (
        Dict[str, float], EarthLocation, SkyCoord, Time):
    """
    Compute the pointing metrics for the specified dataset
    """
    observing_frame = derive_observing_frame(dataset)
    pointing = derive_pointing(dataset)
    time = derive_time_samples(dataset)
    summary = compute_pointing_summary(time, pointing, observing_frame,
                                       min_elevation)
    return summary, observing_frame, pointing, time


def get_antenna_type(dataset: h5py.Dataset):
    antenna_type = dataset.attrs['antenna_filter'][:3]
    return antenna_type


def elevation_metrics(dataset, sun_limits_lba=(20, 30),
                      sun_limits_hba=(15, 30),
                      moon_limits_hba=(10, 20),
                      moon_limits_lba=(5, 10),
                      jupiter_limits_lba=(15, 20),
                      jupiter_limits_hba=(5, 10),
                      elevation_limits_lba=(.75, .50),
                      elevation_limits_hba=(.75, .50),
                      min_elevation_hba=30,
                      min_elevation_lba=40,
                      scoring=_separation_to_score) -> Dict:
    antenna_type = get_antenna_type(dataset)
    if antenna_type == 'LBA':
        sun_limits = sun_limits_lba
        moon_limits = moon_limits_lba
        jupiter_limits = jupiter_limits_lba
        min_elevation = min_elevation_lba
        elevation_limits = elevation_limits_lba
    elif antenna_type == 'HBA':
        sun_limits = sun_limits_hba
        moon_limits = moon_limits_hba
        jupiter_limits = jupiter_limits_hba
        min_elevation = min_elevation_hba
        elevation_limits = elevation_limits_hba
    else:
        raise NotImplementedError(
            f"Unknown type {antenna_type}, known types [LBA, HBA]")
    pointing_metrics, *_ = compute_pointing_metrics(dataset, min_elevation)

    scores = dict(
        sun_interference=scoring(pointing_metrics['Sun'], sun_limits),
        moon_interference=scoring(pointing_metrics['Moon'],
                                  moon_limits),
        jupiter_interference=scoring(pointing_metrics['Jupiter'],
                                     jupiter_limits),
        elevation_score=scoring(pointing_metrics['elevation_fraction'],
                                elevation_limits)
    )
    return scores

class SmartJSONSerializer(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.bool_):
            return bool(obj)
        elif isinstance(obj, set):
            return sorted(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()
        elif isinstance(obj, QualityScore):
            return obj.value
        else:
            return super().default(obj)

def nan2null(obj):
    """Iteratively replaces NaN floats with None.
    .. Note ::

        After being 'dumped' by the json package, ``None`` shows as ``null`` and ``NaN`` floats are shown as ``NaN``.
        The javascript parser used in cwl needs this to be ``null`` and cannot parse ``NaN``

    """
    if isinstance(obj, dict):
        return {k:nan2null(v) for k,v in obj.items()}
    elif isinstance(obj, list) or isinstance(obj, numpy.ndarray) or isinstance(obj, tuple):
        return [nan2null(v) for v in obj]
    elif isinstance(obj, float) and numpy.isnan(obj):
        return None
    return obj


def store_metrics(out_path: str, metrics: Dict):
    metrics = nan2null(metrics)
    with open(out_path, 'w') as fout:
        json.dump(metrics, fout, indent=4, cls=SmartJSONSerializer)


def plot_elevation(time: Time, pointing: SkyCoord, observing_frame: EarthLocation,
                   min_elevation: float,
                   elevation_plot_path: str):
    f, ax = plt.subplots()
    formatter = DateFormatter('%Y-%m-%d - %H:%M')
    pointing_elevation = compute_altaz(observing_frame, pointing, time)
    elevation_fraction = numpy.sum(pointing_elevation > min_elevation) / time.size
    style = itertools.cycle(['-.', ':', 'dashed', 'dashdot', 'dotted'])
    ax.plot(time.to_datetime(), pointing_elevation,
            label=f'target (elev.frac.: {elevation_fraction:.2f})')

    for target in TARGETS:
        target_elevation, min_separation, target_name = \
            _compute_separation(target,
                                time,
                                pointing,
                                observing_frame)
        ax.plot(time.to_datetime(), target_elevation,
                label=f'{target_name} ({min_separation:.2f})',
                ls=next(style)
                )
    plt.xticks(rotation=-30)
    ax.axhline(y=min_elevation, color='red', label='min elevation')
    ax.axes.xaxis.set_major_formatter(formatter)
    plt.legend()
    ax.set_ylabel('Elevation [deg]')
    plt.tight_layout()
    plt.savefig(elevation_plot_path)
    return elevation_plot_path
