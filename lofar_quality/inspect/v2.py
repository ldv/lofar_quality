import logging
import os

import h5py
import matplotlib
import matplotlib.pyplot as plt
import numpy
import scipy
from matplotlib.ticker import AutoMinorLocator

from .common import LOFAR_CORE_ANTENNAS, LOFAR_REMOTE_ANTENNAS
from .common import QualityScore
from .common import convert_mjs_to_time
from .common import get_antenna_type, compute_pointing_metrics, \
    are_only_dutch, \
    missing_dutch_antennas, are_only_core, missing_antennas, \
    contains_remote, IMPORTANT_REMOTE, is_full_array, is_modern_array, \
    IMPORTANT_INTERNATIONAL_MODERN, IMPORTANT_INTERNATIONAL, plot_elevation, \
    transform_antenna_names_to_station_names, antennas_that_where_not_available_at_date

def compute_rfi_percentage(dataset):
    try:
        return numpy.nanmean(dataset['RFI_PERCENTAGE'][:])
    except KeyError:
        return numpy.NaN


def scoring(value: float, low: float, high: float = None,
            reverse: bool = False) -> QualityScore:
    """
    Returns the score give the low and high value

    if high is not provided returns the following comparison

    value < low   returns 'good'
    value >= low  returns 'poor'

    if high is set

    value < low         returns 'good'
    low <= value < high returns 'moderate'
    value > high        returns 'poor'


    if reverse is True it will revert the scores
    """
    if not numpy.isfinite(value):
        return QualityScore.UNKNOWN
    if high is None and value >= low:
        return QualityScore.GOOD if reverse else QualityScore.POOR
    if value < low:
        return QualityScore.POOR if reverse else QualityScore.GOOD
    if low <= value < high:
        return QualityScore.MODERATE
    return QualityScore.GOOD if reverse else QualityScore.POOR


def is_high_rfi(dataset,
                high_threshold_hba=3.5,
                low_threshold_hba=2,
                high_threshold_lba=3,
                low_threshold_lba=1.75
                ):
    """Determine the rfi-quality.
    Note: scoring thresholds defined in https://support.astron.nl/confluence/display/LDV/Imaging+compression+workflow+metrics+v.2
    """
    if get_antenna_type(dataset) == 'LBA':
        low_threshold = low_threshold_lba
        high_threshold = high_threshold_lba
    else:
        low_threshold = low_threshold_hba
        high_threshold = high_threshold_hba

    rfi_percentage = compute_rfi_percentage(dataset)
    return scoring(rfi_percentage, low_threshold, high_threshold)


def elevation_metrics(dataset, sun_limits=(20, None),
                      moon_limits_hba=10,
                      moon_limits_lba=15,
                      jupiter_limits_hba=5,
                      jupiter_limits_lba=15,
                      elevation_limits=.50,
                      min_elevation_hba=30,
                      min_elevation_lba=40):
    """Determine the quality of the target in relation to interference of possible nearby sources.
    Note: scoring thresholds defined in https://support.astron.nl/confluence/display/LDV/Imaging+compression+workflow+metrics+v.2
    """
    antenna_type = get_antenna_type(dataset)

    if antenna_type == 'LBA':
        moon_limits = moon_limits_lba
        jupiter_limits = jupiter_limits_lba
        min_elevation = min_elevation_lba
    elif antenna_type == 'HBA':
        moon_limits = moon_limits_hba
        jupiter_limits = jupiter_limits_hba
        min_elevation = min_elevation_hba
    else:
        raise NotImplementedError()
    pointing_metrics, *_ = compute_pointing_metrics(dataset, min_elevation)

    scores = dict(
        sun_interference=scoring(pointing_metrics['Sun'], *sun_limits, reverse=True),
        moon_interference=scoring(pointing_metrics['Moon'],
                                  moon_limits, reverse=True),
        jupiter_interference=scoring(pointing_metrics['Jupiter'],
                                     jupiter_limits, reverse=True),
        elevation_score=scoring(pointing_metrics['elevation_fraction'],
                                elevation_limits, reverse=True)
    )
    return scores, pointing_metrics


def degree_incompleteness(antenna_names):
    if are_only_core(antenna_names):
        return 'NA'
    if are_only_dutch(antenna_names):
        return missing_dutch_antennas(antenna_names)

    return missing_antennas(antenna_names)


def array_missing_important_pairs_is(antenna_names):
    if are_only_dutch(antenna_names):
        return 'NA'
    if is_full_array(antenna_names) and is_modern_array(antenna_names):
        if (len(IMPORTANT_INTERNATIONAL_MODERN.difference(antenna_names)) > 0) or \
                (len(IMPORTANT_INTERNATIONAL.difference(antenna_names) > 0)):
            return QualityScore.POOR
        else:
            return QualityScore.GOOD
    elif is_full_array(antenna_names) and not is_modern_array(antenna_names):
        if len(IMPORTANT_INTERNATIONAL.difference(antenna_names)) > 0:
            return QualityScore.POOR
        else:
            return QualityScore.GOOD


def array_missing_important_remote_pair(antenna_names):
    if contains_remote(antenna_names) and len(IMPORTANT_REMOTE.difference(
            set(antenna_names))) > 0:
        return QualityScore.POOR
    else:
        return QualityScore.GOOD

def aggregated_array_data_losses_percentage(dataloss_per_antenna):
    if dataloss_per_antenna is None:
        return QualityScore.UNKNOWN
    return scoring(dataloss_per_antenna.mean(), 5, 10)


def dataloss_per_antenna(dataset):
    try:
        expected_visibility_counts = dataset['VISIBILITY_COUNTS'][:].max()
        dl_per_antenna = (1 - dataset['VISIBILITY_COUNTS'] / expected_visibility_counts) * 100
    except:
        dl_per_antenna = None
    return dl_per_antenna


def _get_id_of_selected_antennas(antenna_names, selection):
    return [idx for idx, antenna_names in enumerate(antenna_names) if
            antenna_names in selection]


def _is_dataloss_higher_then_limit_in_selection(dataloss, antenna_names, selection,
                                                limit):
    important_antennas = _get_id_of_selected_antennas(antenna_names, selection)
    return dataloss[important_antennas].max() > limit


def dataloss_on_important_pair(antenna_names, dataloss, limit):
    if are_only_core(antenna_names):
        return 'NA'

    if _is_dataloss_higher_then_limit_in_selection(dataloss, antenna_names,
                                                   IMPORTANT_REMOTE, limit):
        return QualityScore.MODERATE
    else:
        return QualityScore.GOOD


def dataloss_in_important_is_pair(antenna_names, dataloss, limit=.25):
    if are_only_dutch(antenna_names):
        return 'NA'
    if is_full_array(antenna_names) and is_modern_array(antenna_names):
        if _is_dataloss_higher_then_limit_in_selection(dataloss, antenna_names,
                                                       IMPORTANT_INTERNATIONAL_MODERN,
                                                       limit) and \
                _is_dataloss_higher_then_limit_in_selection(dataloss, antenna_names,
                                                            IMPORTANT_INTERNATIONAL,
                                                            limit):

            return QualityScore.POOR
        else:
            return QualityScore.GOOD

    elif is_full_array(antenna_names) and not is_modern_array(antenna_names):
        if _is_dataloss_higher_then_limit_in_selection(dataloss, antenna_names,
                                                       IMPORTANT_INTERNATIONAL,
                                                       limit):
            return QualityScore.POOR
        else:
            return QualityScore.GOOD

def array_high_data_loss_on_is_important_pair(antenna_names, dataloss_per_antenna):
    if are_only_dutch(antenna_names):
        return 'NA'
    if dataloss_per_antenna is None or antenna_names is None:
        return QualityScore.UNKNOWN
    antenna_dataloss = {name: loss for (name, loss) in zip(antenna_names, dataloss_per_antenna)}
    for antenna in IMPORTANT_INTERNATIONAL_MODERN.union(IMPORTANT_INTERNATIONAL):
        try:
            if antenna_dataloss[antenna] > 25:
                return QualityScore.MODERATE
        except KeyError:
            pass
    return QualityScore.GOOD

def array_high_data_loss_on_dutch_important_pair(antenna_names, dataloss_per_antenna):

    if contains_remote(antenna_names) and not any([a in antenna_names for a in IMPORTANT_REMOTE]):
        return 'NA'
    if dataloss_per_antenna is None or antenna_names is None:
        return QualityScore.UNKNOWN
    antenna_dataloss = {name: loss for (name, loss) in zip(antenna_names, dataloss_per_antenna)}
    for antenna in IMPORTANT_REMOTE:
        try:
            if antenna_dataloss[antenna] > 25:
                return QualityScore.MODERATE
        except KeyError:
            pass
    return QualityScore.GOOD


def antenna_configuration(antenna_names):
    if are_only_core(antenna_names):
        return 'CORE'
    if are_only_dutch(antenna_names):
        return 'DUTCH'
    return 'FULL'


def fix_antenna_to_avoid_underscoring_unavailable_at_timestamp(antenna_names,
                                                               timestamp):
    unavailable_antennas = antennas_that_where_not_available_at_date(timestamp)
    antenna_names = set(antenna_names)
    if are_only_core(antenna_names):
        antenna_names.update(unavailable_antennas.intersection(LOFAR_CORE_ANTENNAS))
    elif are_only_dutch(antenna_names):
        antenna_names.update(unavailable_antennas.intersection(LOFAR_CORE_ANTENNAS))
        antenna_names.update(unavailable_antennas.intersection(LOFAR_REMOTE_ANTENNAS))
    else:
        antenna_names.update(unavailable_antennas)

    return list(antenna_names)


def extract_quality_metrics(
        path_to_dataset,
        rfi_low_lba = 0,
        rfi_low_hba = 0,
        rfi_high_lba = 2.75,
        rfi_high_hba = 2.75,
        median_dataloss_low = 0,
        median_dataloss_high = 1.5,
        sun_low=20,
        moon_low_hba=10,
        moon_low_lba=15,
        jupiter_low_hba=5,
        jupiter_low_lba=15,
        elevation_frac=.50,
        min_elevation_hba=30,
        min_elevation_lba=40
    ):
    dataset = h5py.File(path_to_dataset)['/DATASET']
    observation_start_time = convert_mjs_to_time(
        dataset.attrs['obs_start_time']).to_datetime()
    antenna_names = transform_antenna_names_to_station_names(
        dataset.attrs['antenna_names'])
    dataloss_per_a = dataloss_per_antenna(dataset)

    unavailable_antennas = antennas_that_where_not_available_at_date(
        observation_start_time)

    antenna_names = fix_antenna_to_avoid_underscoring_unavailable_at_timestamp(
        antenna_names,
        observation_start_time)

    try:
        total_count = dataset['VISIBILITY_COUNTS'][:].max()
        counts_perc = 100. * (1 - dataset['VISIBILITY_COUNTS'][:] / total_count)
        median_dataloss = numpy.nanmedian(counts_perc)
    except:
        median_dataloss = numpy.nan

    # IMPORTANT NOTE:
    # ANY THRESHOLD VALUE THAT IS REPLACED HERE SHOULD ALSO BE REPLACED IN THE quality_indicators dict!!
    #
    metrics = {
        # RFI is poor if > 2.75, else moderate
        'rfi_perc_total': is_high_rfi(
            dataset,
            high_threshold_hba=rfi_high_hba,
            low_threshold_hba=rfi_low_hba,
            high_threshold_lba=rfi_high_lba,
            low_threshold_lba=rfi_low_lba,
        ),
        'degree_incompleteness_array': degree_incompleteness(antenna_names),
        'array_missing_important_pairs_dutch': array_missing_important_remote_pair(
            antenna_names),
        'array_missing_important_pairs_is': array_missing_important_pairs_is(
            antenna_names),
        'aggregated_array_data_losses_percentage': aggregated_array_data_losses_percentage(dataloss_per_a),
        'array_high_data_loss_on_dutch_important_pair':
            array_high_data_loss_on_dutch_important_pair(antenna_names, dataloss_per_a),
        'array_high_data_loss_on_is_important_pair': array_high_data_loss_on_is_important_pair(antenna_names, dataloss_per_a),
        'is_high_median_dataloss': scoring(median_dataloss, median_dataloss_low, median_dataloss_high),
    }
    scores, pointing_metrics = elevation_metrics(
        dataset,
        sun_limits=(sun_low, None),
        moon_limits_hba=moon_low_hba,
        moon_limits_lba=moon_low_lba,
        jupiter_limits_hba=jupiter_low_hba,
        jupiter_limits_lba=jupiter_low_lba,
        elevation_limits=elevation_frac,
        min_elevation_hba=min_elevation_hba,
        min_elevation_lba=min_elevation_lba
    )
    metrics.update(scores)
    try:
        dsum = dataset['DSum'][:]
        dsum_p2 = dataset['DSumP2'][:]
        dcount = dataset['DCount'][:]
        dcount_full = numpy.array([dcount[:, k // 2] for k in range(dsum.shape[1])]).swapaxes(0, 1)
        sum_mean_squared = dsum * dsum / dcount_full
        residual = (dsum_p2 - sum_mean_squared) / dcount_full
        dstdev = numpy.sqrt(residual)
        noise = dstd_to_noise(dstdev)
    except:
        noise = [numpy.nan] * len(antenna_names)

    metrics['details'] = {
        'rfi_percent': compute_rfi_percentage(dataset),
        'median_dataloss': median_dataloss,
        'target': dataset.attrs['target'],
        'pointing': pointing_metrics,
        'antennas': dataset.attrs['antenna_names'],
        'antenna_configuration': antenna_configuration(antenna_names),
        'antennas_not_available': unavailable_antennas,
        'DStDev': {
            antenna_name: float(value)
            for antenna_name, value in
            zip(dataset.attrs['antenna_names'], noise)
        }
    }

    # Determine overarching/summary quality flag
    overall_quality = -1
    quality_to_int = {
        "unknown": -1,
        "n/a": -1,
        "good": 0,
        "moderate": 1,
        "poor": 2,
    }

    # Deliberately only take sun moon and jupiter into account.
    # Please refer to: https://support.astron.nl/confluence/display/~iacobelli/2024-11-28+IF+comp+metrics
    for score_name in (
        "rfi_perc_total",
        "is_high_median_dataloss",
        "sun_interference",
        "moon_interference",
        "jupiter_interference",
    ):
        quality = metrics[score_name]
        if isinstance(quality, QualityScore):
            quality_value = quality_to_int[quality.value]
        elif isinstance(quality, str) and quality.upper() == "N/A":
            quality_value = quality_to_int[quality]
        else:
            logging.warning(f"Encountered unrecognized quality score: {quality} for metric {score_name}")
        overall_quality = max(overall_quality, quality_value)

    if overall_quality == -1:
        metrics["details"]["quality"] = QualityScore.UNKNOWN
    elif overall_quality == 0:
        # Note: quality is never GOOD at this stage, but always MODERATE at best.
        # Decision was made in the following document: https://support.astron.nl/confluence/display/~iacobelli/2024-11-28+IF+comp+metrics
        metrics["details"]["quality"] = QualityScore.MODERATE
    elif overall_quality == 1:
        metrics["details"]["quality"] = QualityScore.MODERATE
    elif overall_quality == 2:
        metrics["details"]["quality"] = QualityScore.POOR
    else:
        raise ValueError(f"Unexpected quality value of {overall_quality}")

    if get_antenna_type(dataset) == 'LBA':
        metrics["details"]["quality_indicators"] = dict(
            rfi_limit = rfi_high_lba,
            median_dataloss_limit = median_dataloss_high,
            sun_angle_limit = sun_low,
            moon_angle_limit = moon_low_lba,
            jupiter_angle_limit = jupiter_low_lba,
        )
    else:
        metrics["details"]["quality_indicators"] = dict(
            rfi_limit = rfi_high_hba,
            median_dataloss_limit = median_dataloss_high,
            sun_angle_limit = sun_low,
            moon_angle_limit = moon_low_hba,
            jupiter_angle_limit = jupiter_low_hba,
        )
    return metrics


def plot_dataloss(dataset, outfile):
    """
    plot antennas counts (datalosses, if assumed that exposure was complete)
    """

    antennae_names = dataset.attrs['antenna_names']  # get station names for plotting
    try:
        total_count = dataset['VISIBILITY_COUNTS'][:].max()
        counts_perc = 100. * (1 - dataset['VISIBILITY_COUNTS'][:] / total_count)

        fig, ax = plt.subplots(figsize=(10, 6))
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        plt.bar(antennae_names, counts_perc)
        plt.title(f'Median dataloss: {numpy.nanmedian(counts_perc)}, std: {numpy.nanstd(counts_perc)}, weighted_sum: {numpy.nansum(counts_perc / len(counts_perc))}')
        plt.xticks(rotation='vertical')
        plt.xlabel('Antenna name')
        plt.ylabel('Data loss counts [%]')
        plt.tight_layout()
        plt.savefig(outfile)
        return outfile
    except Exception as e:
        logging.warning(f"Skipping data loss plot due to insufficient metadata. Error: {e}")
        return None


def dstd_to_noise(noise_dstdev):
    """
    The error is a complex value extract the norm
    """
    std_xx = numpy.sqrt(
        numpy.power(noise_dstdev[:, 1], 2) +
        numpy.power(noise_dstdev[:, 2], 2))
    std_yy = numpy.sqrt(
        numpy.power(noise_dstdev[:, -2], 2) +
        numpy.power(noise_dstdev[:, -1], 2))
    noise_dstdev = (std_xx + std_yy) / 2.
    return noise_dstdev


def plot_antenna_noise(dataset, outfile):
    """
    Plot antenna's noise content
    """
    try:
        antenna_names = dataset.attrs['antenna_names']
        dsum = dataset['DSum'][:]
        dsum_p2 = dataset['DSumP2'][:]
        dcount = dataset['DCount'][:]
        dcount_full = numpy.array([dcount[:, k // 2] for k in range(dsum.shape[1])]).swapaxes(
            0, 1)

        sum_mean_squared = dsum * dsum / dcount_full
        residual = (dsum_p2 - sum_mean_squared) / dcount_full
        noise_dstdev = numpy.sqrt(residual)
        noise_dstdev = dstd_to_noise(noise_dstdev)

        fig, ax = plt.subplots(figsize=(10, 8))
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        plt.bar(antenna_names, noise_dstdev)
        plt.xticks(rotation='vertical')
        plt.xlabel('Antenna name')
        plt.ylabel('Differential standard deviation')
        median = numpy.nanmedian(noise_dstdev)
        std = numpy.nanstd(noise_dstdev)
        plt.title(f'Median: {median}, std: {std}, std [% of median]: {100*std/median}')
        plt.tight_layout()
        plt.savefig(outfile)
        return outfile
    except Exception as e:
        logging.warning(f"Skipping antenna noise plot due to insufficient metadata. Error: {e}")

def plot_flagging_percentage(dataset, outfile):
    # plot the total (RFI) flagged percentage content

    try:
        RFIpercentage = dataset['RFI_PERCENTAGE'][:]
        frequencies = dataset['FREQUENCIES'][:]
        RFIpercentage[numpy.isnan(RFIpercentage)] = 0
        RFIpercentage, frequencies = scipy.signal.resample(RFIpercentage, 500,
                                                        t=frequencies)
        RFIpercentage[RFIpercentage < 0] = 0
        _ = plt.figure(figsize=(10, 6))
        plt.plot(frequencies, RFIpercentage, color='red', linewidth=2.,
                linestyle='-', label='RFI pol I')
        plt.title('Median RFI percentage {}'.format(compute_rfi_percentage(dataset)))
        plt.xlabel('Frequency [MHz]')
        plt.ylabel('RFI pol I [%]')
        plt.minorticks_on()
        plt.grid()
        plt.tight_layout()
        plt.savefig(outfile)
        return outfile
    except Exception as e:
        logging.warning(f"Skipping Median RFI percentage plot due to insufficient metadata. Error: {e}")


def plot_inspect_ds(path_to_dataset, out_path, min_elevation):
    dataset = h5py.File(path_to_dataset)['/DATASET']
    os.makedirs(out_path, exist_ok=True)
    basename = f"{dataset.attrs['target'][0]}_{dataset.attrs['obs_id']}"

    logging.info('plotting dataloss')
    dataloss_path = plot_dataloss(dataset, os.path.join(out_path, f'{basename}_dataloss_antennae.png'))

    logging.info('plotting flagging percentage')
    flagging_percentage_path = plot_flagging_percentage(dataset, os.path.join(out_path,
                                                   f'{basename}_percflagged_antennae.png'))

    logging.info('plotting antenna noise')
    antanna_noise_path = plot_antenna_noise(dataset,
                       os.path.join(out_path, f'{basename}_noise_antennae.png'))

    summary, observing_frame, pointing, time = compute_pointing_metrics(dataset,
                                                                        min_elevation)
    elevation_path = plot_elevation(time, pointing, observing_frame, min_elevation,
                   os.path.join(out_path, f'{basename}_pointing.png'))

    plot_paths = [path for path in (dataloss_path, flagging_percentage_path, antanna_noise_path, elevation_path) if path is not None]
    return plot_paths
