import json
import logging
import os

import h5py
import matplotlib
import numpy
from matplotlib import pyplot as plt
from scipy.constants import c as light_speed

from .common import compute_pointing_metrics, compute_pointing_summary, \
    elevation_metrics
from .common import transform_antenna_names_to_station_names, \
    missing_international_antennas, are_only_dutch, is_full_array, \
    missing_dutch_antennas, store_metrics, plot_elevation

matplotlib.use('Agg')


def compute_quantity_per_antenna_frequency(dataset, quantity_name):
    """
    Compute the quantity specified in quantity name per antenna and frequency given a dataset
    return: quantity_per_antenna_frequency, antennas, frequencies
    rtype: tuple
    """
    quantity = dataset[quantity_name]
    logging.debug('sorting antenna names')
    antennas = sorted(
        set([antenna.replace('HBA0', '').replace('HBA1', '').replace('HBA', '')
             for antenna in dataset.attrs['antenna_names']]))
    logging.debug('antenna names sorted')
    baselines = dataset['BASELINES'][:]
    frequencies = dataset['FREQUENCIES'][:]
    total_counts = numpy.sum(dataset['COUNTS_PER_SAMPLE'][:])
    n_antennas = len(antennas)
    n_frequencies = len(frequencies)
    n_channels = quantity.shape[3]
    freq_axis_length = 100
    if n_frequencies < freq_axis_length:
        freq_axis_length = n_frequencies
    freq_axis_chunk = int(n_frequencies / freq_axis_length)
    frequencies_axis = numpy.linspace(frequencies[0], frequencies[-1], freq_axis_length)
    quantity_per_antenna_frequency = numpy.zeros((n_antennas, freq_axis_length))
    for antenna_index, antenna in enumerate(antennas):

        logging.info('processing antenna %s', antenna)
        logging.debug('processing antenna %s -making mask', antenna)
        baselines_mask = [index for index, baseline in enumerate(baselines) if
                          antenna in baseline.decode()]
        logging.info('processing antenna %s -mask done', antenna)
        for frequency_index in range(freq_axis_length):
            start_freq = freq_axis_chunk * frequency_index
            end_freq = freq_axis_chunk * (frequency_index + 1)
            end_freq = end_freq if end_freq < n_frequencies else n_frequencies
            total_count_per_iteration = numpy.sum(baselines_mask) * total_counts * (
                    end_freq - start_freq) * n_channels

            quantity_per_antenna_frequency[antenna_index, frequency_index] = \
                quantity[:, baselines_mask, start_freq:end_freq,
                :].sum() / total_count_per_iteration

    return quantity_per_antenna_frequency, antennas, frequencies_axis


def compute_data_loss_per_antenna_frequency(dataset):
    """
    Compute the data loss per antenna and frequency given a dataset
    return: quantity_per_antenna_frequency, antennas, frequencies
    rtype: tuple
    """
    return compute_quantity_per_antenna_frequency(dataset, 'DATALOSS')


def make_data_loss_per_frequency_station(dataset, out_path):
    data_loss_per_frequency_antenna, antennas, frequencies = compute_data_loss_per_antenna_frequency(
        dataset)
    n_antennas = len(antennas)
    n_frequencies = len(frequencies)

    frequency_labels = ['%.3f MHz' % (frequency / 1.e6) for frequency in frequencies]
    plt.figure(figsize=(30, 10))

    plt.imshow(data_loss_per_frequency_antenna,
               extent=[0, n_frequencies, 0, n_antennas], vmin=0, vmax=1, cmap='jet')
    plt.suptitle('Dataloss percentage')
    plt.yticks(ticks=numpy.linspace(0, n_antennas - 1, n_antennas) + .5,
               labels=antennas)
    plt.xticks(ticks=numpy.linspace(0, n_frequencies - 1, n_frequencies) + .5,
               labels=frequency_labels, rotation=90)
    plt.colorbar()
    plt.savefig(out_path)
    plt.close()


def compute_flagging_per_antenna_frequency(dataset):
    """
    Compute the data loss per antenna and frequency given a dataset
    return: quantity_per_antenna_frequency, antennas, frequencies
    rtype: tuple
    """
    return compute_quantity_per_antenna_frequency(dataset, 'FLAG')


def make_flagging_per_frequency_station(dataset, out_path):
    flagging_per_frequency_antenna, antennas, frequencies = compute_flagging_per_antenna_frequency(
        dataset)
    n_antennas = len(antennas)
    n_frequencies = len(frequencies)
    frequency_labels = ['%.3f MHz' % (frequency / 1.e6) for frequency in frequencies]
    plt.figure(figsize=(30, 10))
    plt.imshow(flagging_per_frequency_antenna, extent=[0, n_frequencies, 0, n_antennas],
               vmin=0, vmax=1., cmap='jet')
    plt.suptitle('Flag percentage')
    plt.yticks(ticks=numpy.linspace(0, n_antennas - 1, n_antennas) + .5,
               labels=antennas)
    plt.xticks(ticks=numpy.linspace(0, n_frequencies - 1, n_frequencies) + .5,
               labels=frequency_labels, rotation=90)
    plt.colorbar()
    plt.savefig(out_path)
    plt.close()


def make_missing_data_per_frequency(dataset):
    """Calculate the missing/flagged data per frequency"""
    data_loss = dataset['DATALOSS']
    flagging = dataset['FLAG']
    frequencies = dataset['FREQUENCIES'][:]
    counts = dataset['COUNTS_PER_SAMPLE'][:]
    n_frequencies = len(frequencies)
    data_loss_per_frequency = numpy.zeros(n_frequencies)
    flagging_per_frequency = numpy.zeros(n_frequencies)
    for freq in range(len(frequencies)):
        data_loss_per_frequency[freq] = numpy.mean(data_loss[:, :, freq, :],
                                                   axis=(0, 1, 2)) / counts.sum()
        flagging_per_frequency[freq] = numpy.mean(flagging[:, :, freq, :],
                                                  axis=(0, 1, 2)) / counts.sum()

    return frequencies, data_loss_per_frequency, flagging_per_frequency


def plot_missing_data_per_frequency(frequencies, data_loss_per_frequency,
                                    flagging_per_frequency, out_path):
    """Creates plot for missing/flagged data per frequency"""
    plt.suptitle('Missing/Flagged data per frequency')
    plt.plot(frequencies, data_loss_per_frequency, label='dataloss')
    plt.plot(frequencies, flagging_per_frequency, label='flagging')
    plt.ylim(0, 1)
    plt.legend()
    plt.savefig(out_path)


def _make_coverage_plot(dataset, out_path, quantity):
    quantity = numpy.array(dataset[quantity], dtype=float)
    sampling = 200
    freqs = dataset['FREQUENCIES']
    total_counts = dataset['COUNTS_PER_SAMPLE']

    u_max = numpy.max(dataset['UVW'][:, :, 0])
    u_min = -u_max

    v_max = numpy.max(dataset['UVW'][:, :, 1])
    v_min = -v_max

    uv_max = numpy.max((u_max, v_max, u_min, v_min)) + 5
    u_max = uv_max
    u_min = -uv_max
    v_max = uv_max
    v_min = -uv_max

    u_max, v_max = numpy.array((u_max, v_max)) * freqs[-1] / light_speed / 1.e3
    u_min, v_min = numpy.array((u_min, v_min)) * freqs[0] / light_speed / 1.e3
    quantity_map = numpy.zeros([sampling, sampling])

    # Perform a mean()
    for k, count in enumerate(total_counts):
        quantity[k, :, :, :] /= count

    n_channels = quantity.shape[3]
    for index, freq in enumerate(freqs):
        uv = dataset['UVW'][:, :, :2] * freq / light_speed / 1.e3
        u = uv[:, :, 0].flatten()
        v = uv[:, :, 1].flatten()

        quantity_item = numpy.sum(quantity[:, :, index, :],
                                  axis=2).flatten() / n_channels

        count_map_item, *_ = numpy.histogram2d(u, v, bins=(sampling, sampling),
                                               range=[[u_min, u_max], [v_min, v_max]])
        total_count_map_item = count_map_item

        count_map_item, *_ = numpy.histogram2d(-u, -v, bins=(sampling, sampling),
                                               range=[[u_min, u_max], [v_min, v_max]])
        total_count_map_item += count_map_item

        quantity_map_item, *_ = numpy.histogram2d(u, v, bins=(sampling, sampling),
                                                  range=[[u_min, u_max],
                                                         [v_min, v_max]],
                                                  weights=quantity_item)
        total_quantity_map_item = quantity_map_item

        quantity_map_item, *_ = numpy.histogram2d(-u, -v, bins=(sampling, sampling),
                                                  range=[[u_min, u_max],
                                                         [v_min, v_max]],
                                                  weights=quantity_item)

        total_quantity_map_item += quantity_map_item
        total_quantity_map_item = numpy.divide(total_quantity_map_item,
                                               total_count_map_item)
        quantity_map += total_quantity_map_item

    quantity_map /= len(freqs)
    plt.figure(figsize=[15, 10])

    plt.imshow(quantity_map, cmap='jet', extent=[u_min, u_max, v_min, v_max],
               origin='lower', vmin=0)

    plt.xlabel('u')
    plt.ylabel('v')
    plt.colorbar()
    plt.savefig(out_path)
    plt.close()


def make_data_loss_coverage_plot(dataset, out_path):
    _make_coverage_plot(dataset, out_path, 'DATALOSS')


def make_flagging_coverage_plot(dataset, out_path):
    _make_coverage_plot(dataset, out_path, 'FLAG')


def make_plot_elevation(path_to_dataset, min_elevation, out_path):
    inspect_ds = h5py.File(path_to_dataset)['/DATASET']

    summary, observing_frame, pointing, time = compute_pointing_metrics(inspect_ds,
                                                                        min_elevation)
    plot_elevation(time, pointing, observing_frame, min_elevation,
                   os.path.join(out_path, 'pointing.png'))

    with open(os.path.join(out_path, 'pointing.json'), 'w') as f_stream:
        summary = compute_pointing_summary(time, pointing, observing_frame,
                                           min_elevation)
        json.dump(summary, f_stream)


def plot_inspect_ds(path_to_dataset, out_path, min_elevation):
    dataset = h5py.File(path_to_dataset)['/DATASET']
    os.makedirs(out_path, exist_ok=True)
    print('dataloss coverage')
    make_data_loss_coverage_plot(dataset,
                                 os.path.join(out_path, 'dataloss_coverage_plot.png'))

    print('flagging coverage')
    make_flagging_coverage_plot(dataset,
                                os.path.join(out_path, 'flagging_coverage_plot.png'))

    print('dataloss frequency-station')
    make_data_loss_per_frequency_station(dataset,
                                         os.path.join(out_path,
                                                      'dataloss_per_frequency_station.png'))
    print('flagging frequency-station')
    make_flagging_per_frequency_station(dataset,
                                        os.path.join(out_path,
                                                     'flagging_per_frequency_station.png'))

    print('missing data per frequency')
    frequencies, data_loss_per_frequency, flagging_per_frequency = make_missing_data_per_frequency(
        dataset)
    plot_missing_data_per_frequency(frequencies, data_loss_per_frequency,
                                    flagging_per_frequency, os.path.join(out_path,
                                                                         'missing_data_per_frequency.png'))

    print('elevation plot')
    make_plot_elevation(path_to_dataset, min_elevation, out_path)


def _aggregate_per_antenna(dataset, transform_function):
    quantity_per_antenna_frequency, antennas, frequencies = transform_function(dataset)
    quantity_per_antenna = {antenna: flagging_percentage for
                            antenna, flagging_percentage in
                            zip(antennas, quantity_per_antenna_frequency.mean(axis=1))}
    return quantity_per_antenna


def _flagging_percentage_per_station(dataset):
    return _aggregate_per_antenna(dataset, compute_flagging_per_antenna_frequency)


def _data_loss_percentage_per_station(dataset):
    return _aggregate_per_antenna(dataset, compute_data_loss_per_antenna_frequency)


def _missing_important_pair(antenna_names):
    return ('RS210' not in antenna_names) and ('RS310' not in antenna_names)


def _missing_is_pair(antenna_names):
    station_names = transform_antenna_names_to_station_names(antenna_names)
    important_polish = {'PL610', 'PL611', 'PL612'}
    missing_polish = len(important_polish.difference(station_names)) == 0
    return missing_polish and (
            ('DE601' not in station_names) or ('DE605' not in station_names))


def is_high_flagging(dataset, high_threshold=.25, low_threshold=.10):
    antenna_filter = dataset.attrs['antenna_filter']
    flagging = numpy.mean(dataset['FLAG'])
    if antenna_filter in ['LBA_10_90', 'LBA_30_90', 'HBA_110_190']:
        return flagging > low_threshold
    elif antenna_filter in ['HBA_170_230', 'HBA_210_250']:
        return flagging > high_threshold


def extract_sensitivity_metrics(dataset, flagging_threshold, data_loss_threshold):
    antenna_names = dataset.attrs['antenna_names']
    flagging_percentage_per_station = _flagging_percentage_per_station(dataset)
    data_loss_percentage_per_station = _data_loss_percentage_per_station(dataset)

    n_station_with_high_flagging = sum(map(lambda x: x >= flagging_threshold,
                                           flagging_percentage_per_station.values()))
    n_station_with_high_data_loss = sum(
        map(lambda x: x >= data_loss_threshold,
            data_loss_percentage_per_station.values()))

    sensitivity_metrics = {
        'dutch_array_incomplete': are_only_dutch(antenna_names) and len(
            missing_dutch_antennas(antenna_names)) > 4,
        'dutch_array_high_data_loss': are_only_dutch(
            antenna_names) and n_station_with_high_data_loss > 4,
        'dutch_array_flag_data_loss': are_only_dutch(
            antenna_names) and n_station_with_high_flagging > 4,

        'dutch_array_missing_important_pair': are_only_dutch(
            antenna_names) and _missing_important_pair(antenna_names),
        'dutch_array_high_data_loss_on_important_pair': (
                                                                'RS210' not in flagging_percentage_per_station) or
                                                        (
                                                                'RS310' not in flagging_percentage_per_station) or
                                                        ((
                                                                 flagging_percentage_per_station[
                                                                     'RS210'] > flagging_threshold) and (
                                                                 flagging_percentage_per_station[
                                                                     'RS310'] > flagging_threshold)) or
                                                        ((
                                                                 data_loss_percentage_per_station[
                                                                     'RS210'] > data_loss_threshold) and (
                                                                 data_loss_percentage_per_station[
                                                                     'RS310'] > data_loss_threshold)),

        'full_array_incomplete': is_full_array(antenna_names) and (
                len(missing_dutch_antennas(antenna_names)) > 4 or
                len(missing_international_antennas(antenna_names)) > 2
        ),
        'full_array_missing_important_pair': is_full_array(antenna_names) and (
            _missing_important_pair(antenna_names)
        ),
        'full_array_incomplete_is': is_full_array(antenna_names) and (
                len(missing_international_antennas(antenna_names)) < 6
        ),
        'fill_array_missing_is_pair': _missing_is_pair(antenna_names)
    }

    return sensitivity_metrics


def extract_quality_metrics(path_to_dataset, out_path,
                            flagging_threshold_high,
                            flagging_threshold_low,
                            flagging_threshold_per_station, data_loss_threshold):
    dataset = h5py.File(path_to_dataset)['/DATASET']
    metrics = {
        'high_flagging': is_high_flagging(dataset,
                                          high_threshold=flagging_threshold_high,
                                          low_threshold=flagging_threshold_low)
    }
    metrics.update(extract_sensitivity_metrics(dataset, flagging_threshold_per_station,
                                               data_loss_threshold))
    metrics.update(elevation_metrics(dataset))
    store_metrics(out_path, metrics)
