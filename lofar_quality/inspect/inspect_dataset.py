import h5py

from lofar_quality.common import LEGACY_INSPECT_DS_VERSION, INSPECT_DS_VERSION
from .v1 import extract_quality_metrics as extract_quality_metrics_v1
from .v1 import plot_inspect_ds as plot_inspect_ds_v1
from .v2 import extract_quality_metrics as extract_quality_metrics_v2
from .v2 import plot_inspect_ds as plot_inspect_ds_v2
from .common import store_metrics


def extract_quality_metrics(path_to_dataset, out_path,
                            flagging_threshold_high,
                            flagging_threshold_low,
                            flagging_threshold_per_station, data_loss_threshold):
    first_ds = h5py.File(path_to_dataset, 'r')
    version = first_ds.attrs['VERSION']
    if version == LEGACY_INSPECT_DS_VERSION:
        extract_quality_metrics_v1(path_to_dataset, out_path,
                                   flagging_threshold_high,
                                   flagging_threshold_low,
                                   flagging_threshold_per_station, data_loss_threshold)
    elif version == INSPECT_DS_VERSION:
        metrics = extract_quality_metrics_v2(path_to_dataset,
                                   flagging_threshold_high,
                                   flagging_threshold_low,
                                   flagging_threshold_per_station, data_loss_threshold)
        store_metrics(out_path, metrics)
    else:
        raise Exception(f'Invalid dataset version {version}')


def plot_inspect_ds(path_to_dataset, out_path, min_elevation):
    first_ds = h5py.File(path_to_dataset, 'r')
    version = first_ds.attrs['VERSION']
    if version == LEGACY_INSPECT_DS_VERSION:
        plot_inspect_ds_v1(path_to_dataset, out_path, min_elevation)
    elif version == INSPECT_DS_VERSION:
        return plot_inspect_ds_v2(path_to_dataset, out_path, min_elevation)
    else:
        raise Exception(f'Invalid dataset version {version}')
