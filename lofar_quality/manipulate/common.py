import logging
import numpy
import h5py
from .v1 import join_data as join_data_v1
from .v2 import join_data as join_data_v2

from lofar_quality.common import LEGACY_INSPECT_DS_VERSION, INSPECT_DS_VERSION

def order_paths_by_dataset_frequency(paths):
    path_to_freq = []
    for path in paths:
        with h5py.File(path) as f_in:
            try:
                freq = f_in['/DATASET/FREQUENCIES'][0]
            except:
                freq = numpy.inf
            path_to_freq.append((path, freq))
    return [path_freq[0] for path_freq in sorted(path_to_freq, key=lambda x: x[1])]


def join_metadata(paths, output_ds):
    with h5py.File(paths[0], 'r') as first_dataset:
        output_ds.attrs['VERSION'] = first_dataset.attrs['VERSION']
        output_ds.create_group('DATASET')
        dataset: h5py.Group = output_ds['/DATASET']

        dataset.attrs.update(first_dataset['/DATASET'].attrs)
        for chunk_index, path in enumerate(paths[1:]):
            logging.info('processing metadata for chunk %d', chunk_index)
            with h5py.File(path) as f_in:
                dataset.attrs['subbands'] = numpy.concatenate(
                    (dataset.attrs['subbands'], f_in['/DATASET'].attrs['subbands']))


def join_inspect_ds(paths, output):
    paths = order_paths_by_dataset_frequency(paths)
    first_ds = h5py.File(paths[0], 'r')

    output_ds = h5py.File(output, 'w')
    try:
        join_metadata(paths, output_ds)
        if first_ds.attrs['VERSION'] == LEGACY_INSPECT_DS_VERSION:
            join_data_v1(paths, output_ds)
        else:
            join_data_v2(paths, output_ds)
    finally:
        output_ds.close()
        first_ds.close()
