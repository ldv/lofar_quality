import logging

import h5py
import numpy


def extend_or_assign(dataset_a, dataset_b, axis=0, extend=False):
    if dataset_a is None or not numpy.any(numpy.isfinite(dataset_a)):
        try:
            return dataset_b[:]
        except ValueError: # fallback for scalar datasets
            return dataset_b[()]
    elif extend:
        return numpy.append(dataset_a, dataset_b)
    else:
        if dataset_a.ndim == dataset_b.ndim:
            if axis < dataset_a.ndim:
                # Add to existing axis
                return numpy.concatenate((dataset_a, dataset_b), axis=axis)
            else:
                # Create new axis
                return numpy.stack((dataset_a, dataset_b), axis=axis)
        else:
            dataset_b = numpy.expand_dims(dataset_b, axis)
            return extend_or_assign(dataset_a, dataset_b, axis=axis, extend=extend)


SUBBAND_VARIABLES = {'VISIBILITY_COUNTS', 'FREQUENCIES', 'RFI_PERCENTAGE',
                     'VISIBILITY_STD', 'COUNT_PER_TIME', 'DSum', 'DCount', 'DSumP2'}


def join_data(paths, output_ds):
    # function to aggregate the stats per antenna and per frequency over all HDF5 files
    visibility_counts = None
    frequencies = None
    rfi_percentage = None
    visibility_std = None
    count_per_time = None
    dsum = None
    dcount = None
    dsump2 = None
    with h5py.File(paths[0], 'r') as fin:
        for key in fin['/DATASET']:
            if key in SUBBAND_VARIABLES:
                continue
            output_ds[f'/DATASET/{key}'] = numpy.array(fin['/DATASET'][key])

    for indx, path in enumerate(paths):
        with h5py.File(path, 'r') as fin:
            for key in fin['/DATASET']:
                if key == "VISIBILITY_COUNTS":
                    visibility_counts_i = fin['/DATASET/VISIBILITY_COUNTS']
                    visibility_counts = extend_or_assign(visibility_counts, visibility_counts_i, axis=1)
                elif key == "FREQUENCIES":
                    frequencies_i = fin['/DATASET/FREQUENCIES']
                    frequencies = extend_or_assign(frequencies, frequencies_i, extend=True)
                elif key == "RFI_PERCENTAGE":
                    rfi_percentage_i = fin['/DATASET/RFI_PERCENTAGE']
                    rfi_percentage = extend_or_assign(rfi_percentage, rfi_percentage_i, extend=True)
                elif key == "VISIBILITY_STD":
                    visibility_std_i = fin['/DATASET/VISIBILITY_STD']
                    visibility_std = extend_or_assign(visibility_std, visibility_std_i, axis=2)
                elif key == "COUNT_PER_TIME":
                    count_per_time_i = fin['/DATASET/COUNT_PER_TIME']
                    count_per_time = extend_or_assign(count_per_time, count_per_time_i, axis=1)
                elif key == "DSum":
                    dsum_i = fin['/DATASET/DSum']
                    dsum = extend_or_assign(dsum, dsum_i, axis=2)
                elif key == "DCount":
                    dcount_i = fin['/DATASET/DCount']
                    dcount = extend_or_assign(dcount, dcount_i, axis=2)
                elif key == "DSumP2":
                    dsump2_i = fin['/DATASET/DSumP2']
                    dsump2 = extend_or_assign(dsump2, dsump2_i, axis=2)
                else:
                    pass

    if visibility_std is not None:
        try:
            visibility_std = numpy.nanmean(visibility_std, axis=2)
        except:
            pass
        output_ds['/DATASET/VISIBILITY_STD'] = visibility_std
    if count_per_time is not None:
        try:
            count_per_time = numpy.nanmedian(count_per_time, axis=1)
        except:
            pass
        output_ds['/DATASET/COUNT_PER_TIME'] = count_per_time
    if dsum is not None:
        try:
            dsum = numpy.nansum(dsum, axis=2)
        except:
            pass
        output_ds['/DATASET/DSum'] = dsum
    if dsump2 is not None:
        try:
            dsump2 = numpy.nansum(dsump2, axis=2)
        except:
            pass
        output_ds['/DATASET/DSumP2'] = dsump2
    if dcount is not None:
        try:
            dcount = numpy.nansum(dcount, axis=2)
        except:
            pass
        output_ds['/DATASET/DCount'] = dcount
    if visibility_counts is not None:
        try:
            visibility_counts = numpy.nansum(visibility_counts, axis=1)
        except:
            pass
        output_ds['/DATASET/VISIBILITY_COUNTS'] = visibility_counts
    if frequencies is not None:
        output_ds['/DATASET/FREQUENCIES'] = frequencies
    if rfi_percentage is not None:
        output_ds['/DATASET/RFI_PERCENTAGE'] = rfi_percentage
