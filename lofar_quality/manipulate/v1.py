import h5py
import numpy
import logging


def join_data(paths, output_ds):
    dataset = output_ds['/DATASET']
    first_dataset = h5py.File(paths[0], 'r')
    first_dataset.copy('/DATASET/TIME', dataset, name='TIME')
    first_dataset.copy('/DATASET/BASELINES', dataset, name='BASELINES')
    first_dataset.copy('/DATASET/UVW', dataset, name='UVW')
    flag_chunk_shape = first_dataset['/DATASET/FLAG'].shape
    n_times, n_baselines, n_frequencies, n_polarization = first_dataset[
        '/DATASET/FLAG'].shape
    dataloss_chunk_shape = first_dataset['/DATASET/DATALOSS'].shape
    counts_chunk_shape = first_dataset['/DATASET/COUNTS_PER_SAMPLE'].shape

    dataset.create_dataset('FLAG', flag_chunk_shape, maxshape=(n_times,
                                                               n_baselines,
                                                               None,
                                                               n_polarization))

    dataset.create_dataset('DATALOSS', dataloss_chunk_shape, maxshape=(n_times,
                                                                       n_baselines,
                                                                       None,
                                                                       n_polarization))
    dataset.create_dataset('FREQUENCIES', [n_frequencies], maxshape=(None,))
    dataset.create_dataset('COUNTS_PER_SAMPLE', counts_chunk_shape, maxshape=(None,))

    dataset['FLAG'][:, :, :, :] = first_dataset['/DATASET/FLAG'][:, :, :, :]
    dataset['DATALOSS'][:, :, :, :] = first_dataset['/DATASET/DATALOSS'][:, :, :, :]
    dataset['FREQUENCIES'][:] = first_dataset['/DATASET/FREQUENCIES'][:]
    dataset['COUNTS_PER_SAMPLE'][:] = first_dataset['/DATASET/COUNTS_PER_SAMPLE'][:]

    for chunk_index, path in enumerate(paths[1:]):
        logging.info('processing chunk %d', chunk_index)
        with h5py.File(path) as f_in:
            dataset.attrs['subbands'] = numpy.concatenate(
                (dataset.attrs['subbands'], f_in['/DATASET'].attrs['subbands']))

            dataset['FLAG'].resize((chunk_index + 1) * n_frequencies, axis=2)
            dataset['DATALOSS'].resize((chunk_index + 1) * n_frequencies, axis=2)
            dataset['FREQUENCIES'].resize((chunk_index + 1) * n_frequencies, axis=0)
            flags_block = f_in['/DATASET/FLAG']
            dataloss_block = f_in['/DATASET/DATALOSS']
            frequencies_block = f_in['/DATASET/FREQUENCIES']
            counts_per_sample_block = f_in['/DATASET/COUNTS_PER_SAMPLE']
            chuck_frequencies_extend = frequencies_block.shape[0]
            lower_chunk_index = chunk_index * n_frequencies
            upper_chunk_index = chunk_index * n_frequencies + chuck_frequencies_extend
            try:
                dataset['FLAG'][:, :, lower_chunk_index: upper_chunk_index,
                :] = flags_block
                dataset['DATALOSS'][:, :, lower_chunk_index: upper_chunk_index,
                :] = dataloss_block
                dataset['FREQUENCIES'][
                lower_chunk_index: upper_chunk_index] = frequencies_block
            except Exception as e:
                logging.error('error in copying chunk into merged dataset:')
                logging.error('n_frequencies: %s', n_frequencies)
                logging.error('chunk dataset: %s', path)
                logging.error('lower/upper chunk %s/%s', lower_chunk_index,
                              upper_chunk_index)
                logging.error('counts per sample size %s',
                              counts_per_sample_block.shape)
                logging.error('chuck frequency extent %s', chuck_frequencies_extend)
                logging.error('chunk sub band number: %s',
                              f_in['/DATASET'].attrs['subbands'])

                logging.exception(e)

