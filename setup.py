from setuptools import setup, find_packages

setup(
    name='lofar_quality',
    version='0.1.0',
    author_email='mancini@astron.nl',
    author='Mattia Mancini',
    scripts=['bin/inspect_utils.py'],
    description='Tools to inspect and generate quality datasets',
    packages=find_packages(include=['lofar_quality', 'lofar_quality.*']),
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    install_requires=[
        'numpy<2.0.0',
        'python-casacore',
        'numba',
        'matplotlib',
        'scipy>=1.11',
        'h5py',
        'astropy'
    ]
)
