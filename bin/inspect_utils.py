#!/usr/bin/env python3
import logging
from argparse import ArgumentParser, Namespace

from lofar_quality.common import INSPECT_DS_VERSION, LEGACY_INSPECT_DS_VERSION
from lofar_quality.create.v1 import create_inspect_ds as create_inspect_ds_v1
from lofar_quality.create.v2 import create_inspect_ds as create_inspect_ds_v2
from lofar_quality.inspect.inspect_dataset import extract_quality_metrics, \
    plot_inspect_ds
from lofar_quality.manipulate.common import join_inspect_ds

FORMAT = '%(levelname)-15s %(asctime)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


def parse_args() -> Namespace:
    parser = ArgumentParser(description='Inspect dataset managing tool')

    subparser = parser.add_subparsers(title='commands',
                                      description='Operation that can be performed on an inspect'
                                                  ' dataset',
                                      dest='command')
    create_parser = subparser.add_parser('create')
    create_parser.add_argument('measurement_set')
    create_parser.add_argument('inspect_dataset')
    create_parser.add_argument(
        '--sampling_window', default=60, help='time sampling window in seconds',
        type=float)
    create_parser.add_argument(
        '--version_select', choices=[LEGACY_INSPECT_DS_VERSION, INSPECT_DS_VERSION],
        default=INSPECT_DS_VERSION
    )
    join_parser = subparser.add_parser('join')
    join_parser.add_argument('inspect_datasets', nargs='+')
    join_parser.add_argument('output_dataset')

    plot_parser = subparser.add_parser('plot')
    plot_parser.add_argument('inspect_dataset', help='input dataset')
    plot_parser.add_argument(
        '--min_elevation', help='minimum elevation', default=15, type=float)
    plot_parser.add_argument('output_path')

    inspect_parser = subparser.add_parser('inspect')
    inspect_parser.add_argument('inspect_dataset', help='input dataset')
    inspect_parser.add_argument('output_path')
    inspect_parser.add_argument(
        '--min_elevation', help='minimum elevation', default=15, type=float)
    inspect_parser.add_argument(
        '--flagging_threshold_per_station', help='flagging threshold per station',
        default=.30, type=float)
    inspect_parser.add_argument(
        '--flagging_threshold_high', help='flagging threshold high', default=.25,
        type=float)
    inspect_parser.add_argument(
        '--flagging_threshold_low', help='flagging threshold low', default=.10,
        type=float)

    inspect_parser.add_argument(
        '--dataloss_threshold', help='dataloss threshold', default=.30, type=float)

    return parser.parse_args()


def main():
    args = parse_args()

    if args.command == 'create':
        if args.version_select == LEGACY_INSPECT_DS_VERSION:
            create_inspect_ds_v1(args.measurement_set, args.inspect_dataset,
                                 time_resolution=args.sampling_window)
        elif args.version_select == INSPECT_DS_VERSION:
            create_inspect_ds_v2(args.measurement_set, args.inspect_dataset)
    elif args.command == 'join':
        join_inspect_ds(args.inspect_datasets, args.output_dataset)
    elif args.command == 'plot':
        plot_inspect_ds(args.inspect_dataset,
                        args.output_path, args.min_elevation)
    elif args.command == 'inspect':
        extract_quality_metrics(args.inspect_dataset, args.output_path,
                                flagging_threshold_high=args.flagging_threshold_high,
                                flagging_threshold_low=args.flagging_threshold_low,
                                flagging_threshold_per_station=args.flagging_threshold_per_station,
                                data_loss_threshold=args.dataloss_threshold
                                )
    else:
        print('Command unrecognized')


if __name__ == '__main__':
    main()
